package net.minebaum.lobbysystem.utils;


import eu.thesimplecloud.api.CloudAPI;
import eu.thesimplecloud.api.player.CloudPlayer;
import net.minebaum.baumapi.api.GuiAPI;
import net.minebaum.baumapi.mysql.MySQLConnector;
import net.minebaum.baumapi.utils.ItemBuilder;
import net.minebaum.baumapi.utils.Skull.Skull;
import net.minebaum.lobbysystem.LobbySystem;
import net.minebaum.playerapi.MBPlayer;
import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class TopStatsArmorstand {

    private static ArmorStand top1gungame;
    private static ArmorStand top2gungame;
    private static ArmorStand top3gungame;
    private static ArmorStand top1buildffa;
    private static ArmorStand top2buildffa;
    private static ArmorStand top3buildffa;
    public static void setTop3armorstands() {
        String[] topPlayers = getTops();

        if(top1gungame != null) {
            top1gungame.remove();
        }
        if(top2gungame != null) {
            top2gungame.remove();
        }
        if(top3gungame != null) {
            top3gungame.remove();
        }
        if(top1buildffa != null) {
            top1buildffa.remove();
        }
        if(top2buildffa != null) {
            top2buildffa.remove();
        }
        if(top3buildffa != null) {
            top3buildffa.remove();
        }

        top1gungame = (ArmorStand) Bukkit.getWorld("world").spawnEntity(LocationManager.getLocation("top1g"), EntityType.ARMOR_STAND);
        top1gungame.setVisible(true);
        top1gungame.setGravity(false);
        top1gungame.setSmall(true);
        top1gungame.setCustomNameVisible(true);
        top1gungame.setHelmet(null);
        top1gungame.setChestplate(new ItemBuilder(Material.DIAMOND_CHESTPLATE, 1, (short)0).build());
        top1gungame.setLeggings(new ItemBuilder(Material.DIAMOND_LEGGINGS, 1, (short)0).build());
        top1gungame.setBoots(new ItemBuilder(Material.DIAMOND_BOOTS, 1, (short)0).build());
        top1gungame.setArms(true);
        top1gungame.setItemInHand(new ItemBuilder(Material.DIAMOND_AXE, 1 , (short)0).build());
        top1gungame.setBasePlate(false);
        top1gungame.setCustomName("§b#1 §7" + topPlayers[GUNGAME_1]);

        top2gungame = (ArmorStand) Bukkit.getWorld("world").spawnEntity(LocationManager.getLocation("top2g"), EntityType.ARMOR_STAND);
        top2gungame.setVisible(true);
        top2gungame.setGravity(false);
        top2gungame.setSmall(true);
        top2gungame.setCustomNameVisible(true);
        top2gungame.setHelmet(null);
        top2gungame.setChestplate(new ItemBuilder(Material.IRON_CHESTPLATE, 1, (short)0).build());
        top2gungame.setLeggings(new ItemBuilder(Material.IRON_LEGGINGS, 1, (short)0).build());
        top2gungame.setBoots(new ItemBuilder(Material.IRON_BOOTS, 1, (short)0).build());
        top2gungame.setArms(true);
        top2gungame.setItemInHand(new ItemBuilder(Material.IRON_AXE, 1 , (short)0).build());
        top2gungame.setBasePlate(false);
        top2gungame.setCustomName("§e#2 §7" + topPlayers[GUNGAME_2]);


        top3gungame = (ArmorStand) Bukkit.getWorld("world").spawnEntity(LocationManager.getLocation("top3g"), EntityType.ARMOR_STAND);
        top3gungame.setVisible(true);
        top3gungame.setGravity(false);
        top3gungame.setSmall(true);
        top3gungame.setCustomNameVisible(true);
        top3gungame.setHelmet(null);
        top3gungame.setChestplate(new ItemBuilder(Material.GOLD_CHESTPLATE, 1, (short)0).build());
        top3gungame.setLeggings(new ItemBuilder(Material.GOLD_LEGGINGS, 1, (short)0).build());
        top3gungame.setBoots(new ItemBuilder(Material.GOLD_BOOTS, 1, (short)0).build());
        top3gungame.setArms(true);
        top3gungame.setItemInHand(new ItemBuilder(Material.GOLD_AXE, 1 , (short)0).build());
        top3gungame.setBasePlate(false);
        top3gungame.setCustomName("§6#3 §7" + topPlayers[GUNGAME_3]);



        top1buildffa = (ArmorStand) Bukkit.getWorld("world").spawnEntity(LocationManager.getLocation("top1b"), EntityType.ARMOR_STAND);
        top1buildffa.setVisible(true);
        top1buildffa.setGravity(false);
        top1buildffa.setSmall(true);
        top1buildffa.setCustomNameVisible(true);
        top1buildffa.setHelmet(null);
        top1buildffa.setChestplate(new ItemBuilder(Material.DIAMOND_CHESTPLATE, 1, (short)0).build());
        top1buildffa.setLeggings(new ItemBuilder(Material.DIAMOND_LEGGINGS, 1, (short)0).build());
        top1buildffa.setBoots(new ItemBuilder(Material.DIAMOND_BOOTS, 1, (short)0).build());
        top1buildffa.setArms(true);
        top1buildffa.setItemInHand(new ItemBuilder(Material.DIAMOND_SWORD, 1 , (short)0).build());
        top1buildffa.setBasePlate(false);
        top1buildffa.setCustomName("§b#1 §7" + topPlayers[BUILDFFA_1]);



        top2buildffa = (ArmorStand) Bukkit.getWorld("world").spawnEntity(LocationManager.getLocation("top2b"), EntityType.ARMOR_STAND);
        top2buildffa.setVisible(true);
        top2buildffa.setGravity(false);
        top2buildffa.setSmall(true);
        top2buildffa.setCustomNameVisible(true);
        top2buildffa.setHelmet(null);
        top2buildffa.setChestplate(new ItemBuilder(Material.IRON_CHESTPLATE, 1, (short)0).build());
        top2buildffa.setLeggings(new ItemBuilder(Material.IRON_LEGGINGS, 1, (short)0).build());
        top2buildffa.setBoots(new ItemBuilder(Material.IRON_BOOTS, 1, (short)0).build());
        top2buildffa.setArms(true);
        top2buildffa.setItemInHand(new ItemBuilder(Material.IRON_SWORD, 1 , (short)0).build());
        top2buildffa.setBasePlate(false);
        top2buildffa.setCustomName("§e#2 §7" + topPlayers[BUILDFFA_2]);


        top3buildffa = (ArmorStand) Bukkit.getWorld("world").spawnEntity(LocationManager.getLocation("top3b"), EntityType.ARMOR_STAND);

        top3buildffa.setVisible(true);
        top3buildffa.setGravity(false);
        top3buildffa.setSmall(true);
        top3buildffa.setCustomNameVisible(true);
        top3buildffa.setHelmet(null);
        top3buildffa.setChestplate(new ItemBuilder(Material.GOLD_CHESTPLATE, 1, (short)0).build());
        top3buildffa.setLeggings(new ItemBuilder(Material.GOLD_LEGGINGS, 1, (short)0).build());
        top3buildffa.setBoots(new ItemBuilder(Material.GOLD_BOOTS, 1, (short)0).build());
        top3buildffa.setArms(true);
        top3buildffa.setItemInHand(new ItemBuilder(Material.GOLD_SWORD, 1 , (short)0).build());
        top3buildffa.setBasePlate(false);
        top3buildffa.setCustomName("§6#3 §7" + topPlayers[BUILDFFA_3]);

        updateArmorStands(getTops());

    }

    public static final int GUNGAME_1 = 0,
                            GUNGAME_2 = 1,
                            GUNGAME_3 = 2,
                            BUILDFFA_1 = 3,
                            BUILDFFA_2 = 4,
                            BUILDFFA_3 = 5;

    public static void updateArmorStands(String[] topPlayers){

        ItemStack i1 = Skull.getPlayerSkull(topPlayers[BUILDFFA_1]);
        top1buildffa.setHelmet(i1);
        ItemStack i2 = Skull.getPlayerSkull(topPlayers[BUILDFFA_2]);
        top2buildffa.setHelmet(i2);
        ItemStack i3 = Skull.getPlayerSkull(topPlayers[BUILDFFA_3]);
        top3buildffa.setHelmet(i3);
        ItemStack i4 = Skull.getPlayerSkull(topPlayers[GUNGAME_1]);
        top1gungame.setHelmet(i4);
        ItemStack i5 = Skull.getPlayerSkull(topPlayers[GUNGAME_2]);
        top2gungame.setHelmet(i5);
        ItemStack i6 = Skull.getPlayerSkull(topPlayers[GUNGAME_3]);
        top3gungame.setHelmet(i6);
    }

    public static String[] getTops(){
        String[] tops = new String[6];

        ResultSet bffatop1 = LobbySystem.bffaStats.query("SELECT UUID FROM Stats ORDER BY POINTS DESC LIMIT 3");
        int bffatop1count = 0;
        try {
            while (bffatop1.next()){
                System.out.println(bffatop1.getString("UUID"));
                tops[bffatop1count] = getPlayerNameByUUID(bffatop1.getString("UUID"));
                bffatop1count++;
            }
        }catch (Exception e1){
            e1.printStackTrace();
        }

        ResultSet bffatop2 = LobbySystem.gungameStats.query("SELECT UUID FROM Stats ORDER BY POINTS DESC LIMIT 3");
        int bffatop2count = 3;
        try {
            while (bffatop2.next()){
                tops[bffatop2count] = getPlayerNameByUUID(bffatop2.getString("UUID"));
                bffatop2count++;
            }
        }catch (Exception e1){
            e1.printStackTrace();
        }

        for(String s : tops){
            System.out.println("[TEST] " + s);
        }


        return tops;
    }

    public static String getPlayerNameByUUID(String uuid){
        String finalString = "";
        /*MySQLConnector connector = new MySQLConnector("web7447.cweb03.gamingweb.de", 3306, "mokbungee", "mok1", "Ce6xNmK1O1theJAk");
        connector.connect();
        ResultSet rs = connector.query("SELECT name FROM Users WHERE UUID='" + uuid + "';");
        try {
            finalString = rs.getString("name");
        }catch (Exception e2){
            System.err.println(e2.getMessage());
        }*/
        String url = "https://api.mojang.com/user/profiles/"+uuid.replace("-", "")+"/names";
        try {
            @SuppressWarnings("deprecation")
            String nameJson = IOUtils.toString(new URL(url));
            JSONArray nameValue = (JSONArray) JSONValue.parseWithException(nameJson);
            String playerSlot = nameValue.get(nameValue.size()-1).toString();
            JSONObject nameObject = (JSONObject) JSONValue.parseWithException(playerSlot);
            return nameObject.get("name").toString();
        } catch (IOException |  org.json.simple.parser.ParseException e) {
            e.printStackTrace();
        }
        return "error";
    }

    public static Inventory openStatsView(String uuid, Entity entity){
        Inventory inv = new GuiAPI().fillerGUI(9, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 15).setDisplayname(" ").build(), entity.getCustomName());
        ArrayList<String> lore = new ArrayList<>();
        lore.add("");
        lore.add("§7Kills §c>> §7" + SQLStats.getKills(uuid));
        lore.add("§7Tode §c>> §7" + SQLStats.getDeaths(uuid));
        lore.add("§7Points §c>> §7" + SQLStats.getPoints(uuid));
        ItemStack is = new ItemBuilder(Material.NETHER_STAR, 1, (short) 0).setDisplayname("§eÜbersicht").setLore(lore).build();
        inv.setItem(4, is);

        return inv;
    }


}