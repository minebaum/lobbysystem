package net.minebaum.lobbysystem.utils;

import eu.thesimplecloud.module.permission.PermissionPool;
import eu.thesimplecloud.module.permission.player.IPermissionPlayer;
import net.minebaum.baumapi.utils.ItemBuilder;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class Shooes {

    public static void setShoes(Player player){
        IPermissionPlayer permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPermissionPlayer(player.getUniqueId());
        ItemStack is = new ItemBuilder(Material.LEATHER_BOOTS, 1, (short) 0).build();
        LeatherArmorMeta meta = (LeatherArmorMeta) is.getItemMeta();
        if (permissionPlayer.hasPermissionGroup("Leitung")) {
            meta.setColor(Color.fromBGR(255, 255, 255));
        } else if (permissionPlayer.hasPermissionGroup("Administrator")) {
            meta.setColor(Color.fromBGR(13, 13, 186));
        } else if (permissionPlayer.hasPermissionGroup("Manager")) {
            meta.setColor(Color.fromBGR(5, 5, 255));
        } else if (permissionPlayer.hasPermissionGroup("SrDev")) {
            meta.setColor(Color.fromBGR(181, 143, 16));
        } else if (permissionPlayer.hasPermissionGroup("SrContent")) {
            meta.setColor(Color.YELLOW);
        } else if (permissionPlayer.hasPermissionGroup("SrBuilder")) {
            meta.setColor(Color.YELLOW);
        } else if (permissionPlayer.hasPermissionGroup("SrSupporter")) {
            meta.setColor(Color.fromBGR(181, 143, 16));
        } else if (permissionPlayer.hasPermissionGroup("Dev")) {
            meta.setColor(Color.fromBGR(181, 143, 16));
        } else if (permissionPlayer.hasPermissionGroup("Content")) {
            meta.setColor(Color.AQUA);
        } else if (permissionPlayer.hasPermissionGroup("Builder")) {
            meta.setColor(Color.YELLOW);
        } else if (permissionPlayer.hasPermissionGroup("Supporter")) {
            meta.setColor(Color.fromBGR(181, 143, 16));
        } else if (permissionPlayer.hasPermissionGroup("JrDev")) {
            meta.setColor(Color.fromBGR(181, 143, 16));
        } else if (permissionPlayer.hasPermissionGroup("JrContent")) {
            meta.setColor(Color.AQUA);
        } else if (permissionPlayer.hasPermissionGroup("JrBuilder")) {
            meta.setColor(Color.YELLOW);
        } else if (permissionPlayer.hasPermissionGroup("JrSupporter")) {
            meta.setColor(Color.fromBGR(181, 143, 16));
        } else if (permissionPlayer.hasPermissionGroup("Freund")) {
            meta.setColor(Color.fromBGR(10, 99, 20));
        } else if (permissionPlayer.hasPermissionGroup("Creator")) {
            meta.setColor(Color.fromBGR(130, 7, 103));
        } else if (permissionPlayer.hasPermissionGroup("Partner")) {
            meta.setColor(Color.fromBGR(92, 73, 12));
        } else if (permissionPlayer.hasPermissionGroup("Baum")) {
            meta.setColor(Color.GREEN);
        } else if (permissionPlayer.hasPermissionGroup("Prime")) {
            meta.setColor(Color.fromBGR(255,	000	,255));
        } else if (permissionPlayer.hasPermissionGroup("Premium")) {
            meta.setColor(Color.YELLOW);
        }
        if (permissionPlayer.getPermissionGroupInfoList().size() == 1){
            return;
        }
        is.setItemMeta(meta);
        player.getInventory().setBoots(is);
    }

}
