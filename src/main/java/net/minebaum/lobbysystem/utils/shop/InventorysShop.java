package net.minebaum.lobbysystem.utils.shop;

import net.minebaum.baumapi.api.GuiAPI;
import net.minebaum.baumapi.utils.ItemBuilder;
import net.minebaum.baumapi.utils.Skull.Skull;
import net.minebaum.lobbysystem.listeners.InvenotryClickListener;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventorysShop {

    public Inventory setstandartShop() {
        ItemStack itemhead = Skull.getCustomSkull("http://textures.minecraft.net/texture/dfa605e25f4fc2cea5a766d79a8bfa290313e45d8f5e957d958a0f33fcb16");
        ItemMeta meta = itemhead.getItemMeta();
        meta.setDisplayName("§cZurück");
        itemhead.setItemMeta(meta);
        Inventory standartshop = new GuiAPI().fillerGUI(9*6, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,(short)15).setDisplayname("  ").build(),"§eShop §8| §7Rang");
        standartshop.setItem(1,new ItemBuilder(Material.STAINED_GLASS_PANE,1,(short)14).setDisplayname(" ").build());
        standartshop.setItem(9,new ItemBuilder(Material.STAINED_GLASS_PANE,1,(short)14).setDisplayname(" ").build());
        standartshop.setItem(10,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(11,new ItemBuilder(Material.STAINED_GLASS_PANE,1,(short)14).build());
        standartshop.setItem(12,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(13,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(14,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(15,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(16,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(18,new ItemBuilder(Material.STAINED_GLASS_PANE,1,(short)14).setDisplayname(" ").build());
        standartshop.setItem(19,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(20,new ItemBuilder(Material.STAINED_GLASS_PANE,1,(short)14).setDisplayname(" ").build());
        standartshop.setItem(21,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(22,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(23,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(24,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(25,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(27,new ItemBuilder(Material.STAINED_GLASS_PANE,1,(short)14).setDisplayname(" ").build());
        standartshop.setItem(28,new ItemBuilder(Material.AIR,1,(short)5).build());
        standartshop.setItem(29,new ItemBuilder(Material.STAINED_GLASS_PANE,1,(short)14).setDisplayname(" ").build());
        standartshop.setItem(30,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(31,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(32,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(33,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(34,new ItemBuilder(Material.AIR,1,(short)0).build());
        standartshop.setItem(37,new ItemBuilder(Material.STAINED_GLASS_PANE,1,(short)14).build());
        standartshop.setItem(45, itemhead);
        standartshop.setItem(48,new ItemBuilder(Material.STAINED_GLASS_PANE,1,(short)14).setDisplayname(" ").build());
        standartshop.setItem(49,new ItemBuilder(Material.GOLD_INGOT,1,(short)0).setDisplayname("§7Ränge").build());
        standartshop.setItem(50,new ItemBuilder(Material.SAPLING,1,(short)0).setDisplayname("§7BäumeAktion").build());
        standartshop.setItem(51,new ItemBuilder(Material.SANDSTONE,1,(short)0).setDisplayname("§7BuildFFA").build());
        standartshop.setItem(52,new ItemBuilder(Material.STAINED_GLASS_PANE,1,(short)14).setDisplayname("").build());
        return standartshop;
    }

    public Inventory bestätInv(Player player, String theme, ShopSite before){
        Inventory inv = new GuiAPI().fillerGUI(36, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 15).setDisplayname(" ").build(), theme);
        ItemStack back = Skull.getCustomSkull("http://textures.minecraft.net/texture/dfa605e25f4fc2cea5a766d79a8bfa290313e45d8f5e957d958a0f33fcb16");
        ItemMeta meta = back.getItemMeta();
        meta.setDisplayName("§cZurück");
        back.setItemMeta(meta);
        inv.setItem(27, back);
        inv.setItem(28, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setDisplayname(" ").build());
        inv.setItem(29, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setDisplayname(" ").build());
        inv.setItem(30, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setDisplayname(" ").build());
        inv.setItem(31, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setDisplayname(" ").build());
        inv.setItem(32, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setDisplayname(" ").build());
        inv.setItem(33, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setDisplayname(" ").build());
        inv.setItem(34, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setDisplayname(" ").build());
        inv.setItem(35, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setDisplayname(" ").build());
        inv.setItem(11, new ItemBuilder(Material.STAINED_CLAY, 1, (short) 14).setDisplayname("§cAblehnen").build());
        inv.setItem(15, new ItemBuilder(Material.STAINED_CLAY, 1, (short) 13).setDisplayname("§aBestätigen").build());
        return inv;
    }

}
