package net.minebaum.lobbysystem.utils.shop;

import eu.thesimplecloud.api.CloudAPI;
import eu.thesimplecloud.api.player.ICloudPlayer;
import eu.thesimplecloud.module.permission.PermissionPool;
import eu.thesimplecloud.module.permission.permission.Permission;
import eu.thesimplecloud.module.permission.player.IPermissionPlayer;
import eu.thesimplecloud.module.permission.player.PlayerPermissionGroupInfo;
import net.minebaum.baumapi.BaumAPI;
import net.minebaum.baumapi.utils.Data;
import net.minebaum.baumapi.utils.ItemBuilder;
import net.minebaum.lobbysystem.mysql.SQLBaum;
import net.minebaum.lobbysystem.utils.InventoryM;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.concurrent.TimeUnit;

public class ShopManager {

    private static ShopSite[] sites;

    public static final int RANK_PREMIUM = 0,
                            RANK_PRIME = 1,
                            RANK_BAUM = 2,
                            BAUM_CHEAP = 3,
                            BAUM_NONCHEAP = 4,
                            BFFA_KIT = 5,
                            BFFA_GADGETS = 6;

    public static void setupAllSites(){
        //SITE 0
        sites = new ShopSite[8];
        sites[RANK_PREMIUM] = new ShopSite("Rang", ShopSite.A_MENU_ITEM, ShopSite.C_MAIN_MENU_ITEM);
        sites[RANK_PREMIUM].setupMenu(ShopData.PREMIUM_MENU_ITEM, ShopData.PRIME_MENU_ITEM, ShopData.BAUM_MENU_ITEM);
        sites[RANK_PRIME] = new ShopSite("Rang", ShopSite.B_MENU_ITEM, ShopSite.C_MAIN_MENU_ITEM);
        sites[RANK_PRIME].setupMenu(ShopData.PREMIUM_MENU_ITEM, ShopData.PRIME_MENU_ITEM, ShopData.BAUM_MENU_ITEM);
        sites[RANK_BAUM] = new ShopSite("Rang", ShopSite.C_MENU_ITEM, ShopSite.C_MAIN_MENU_ITEM);
        sites[RANK_BAUM].setupMenu(ShopData.PREMIUM_MENU_ITEM, ShopData.PRIME_MENU_ITEM, ShopData.BAUM_MENU_ITEM);
        sites[BAUM_CHEAP] = new ShopSite("Bäume Pflanzen", ShopSite.A_MENU_ITEM, ShopSite.B_MAIN_MENU_ITEM);
        sites[BAUM_CHEAP].setupMenu(ShopData.BAUM_CHEAP_MENU_ITEM, new ItemBuilder(Material.AIR, 1, (short) 0).build(), ShopData.BAUM_NORM_MENU_ITEM);
        sites[BAUM_NONCHEAP] = new ShopSite("Bäume Pflanzen", ShopSite.C_MENU_ITEM, ShopSite.B_MAIN_MENU_ITEM);
        sites[BAUM_NONCHEAP].setupMenu(ShopData.BAUM_CHEAP_MENU_ITEM, new ItemBuilder(Material.AIR, 1, (short) 0).build(), ShopData.BAUM_NORM_MENU_ITEM);
        sites[BFFA_KIT] = new ShopSite("Kits", ShopSite.A_MENU_ITEM, ShopSite.C_MAIN_MENU_ITEM);
        sites[BFFA_KIT].setupMenu(ShopData.BFFA_KIT_MENU_ITEM, new ItemBuilder(Material.AIR, 1, (short) 0).build(), ShopData.BFFA_GADGET_MENU_ITEM);
        sites[BFFA_GADGETS] = new ShopSite("Gadgets", ShopSite.C_MENU_ITEM, ShopSite.C_MAIN_MENU_ITEM);
        sites[BFFA_GADGETS].setupMenu(ShopData.BFFA_KIT_MENU_ITEM, new ItemBuilder(Material.AIR, 1, (short) 0).build(), ShopData.BFFA_GADGET_MENU_ITEM);

        sites[RANK_PREMIUM].addFIeldItem(ShopSite.FIELD_7, ShopData.PREMIUM_7_ITEM);
        sites[RANK_PREMIUM].addFIeldItem(ShopSite.FIELD_8, ShopData.PREMIUM_30_ITEM);
        sites[RANK_PREMIUM].addFIeldItem(ShopSite.FIELD_9, ShopData.PREMIUM_90_ITEM);

        sites[RANK_PRIME].addFIeldItem(ShopSite.FIELD_7, ShopData.PRIME_7_ITEM);
        sites[RANK_PRIME].addFIeldItem(ShopSite.FIELD_8, ShopData.PRIME_30_ITEM);
        sites[RANK_PRIME].addFIeldItem(ShopSite.FIELD_9, ShopData.PRIME_90_ITEM);

        sites[RANK_BAUM].addFIeldItem(ShopSite.FIELD_8, ShopData.BAUM_7_ITEM);

        sites[BAUM_CHEAP].addFIeldItem(ShopSite.FIELD_8, ShopData.BAUM_CHEAP_FIELD_ITEM);

        sites[BAUM_NONCHEAP].addFIeldItem(ShopSite.FIELD_8, ShopData.BAUM_NORM_FIELD_ITEM);

        sites[BFFA_GADGETS].addFIeldItem(ShopSite.FIELD_3, ShopData.ANGEL_GADGET_ITEM);
        sites[BFFA_GADGETS].addFIeldItem(ShopSite.FIELD_7, ShopData.ENDERPERLE_GADGET_ITEM);
        sites[BFFA_GADGETS].addFIeldItem(ShopSite.FIELD_8, ShopData.SCHNEEBALL_GADGET_ITEM);
        sites[BFFA_GADGETS].addFIeldItem(ShopSite.FIELD_9, ShopData.SPEED_GADGET_ITEM);

        sites[BFFA_KIT].addFIeldItem(ShopSite.FIELD_3, ShopData.ANGREIFER_KIT_ITEM);
        sites[BFFA_KIT].addFIeldItem(ShopSite.FIELD_7, ShopData.PYRO_KIT_ITEM);
        sites[BFFA_KIT].addFIeldItem(ShopSite.FIELD_8, ShopData.TANK_KIT_ITEM);
        sites[BFFA_KIT].addFIeldItem(ShopSite.FIELD_9, ShopData.SPÄHER_KIT_ITEM);
    }

    public static void openShopSite(int siteID, Player target)
    {
        target.openInventory(
                sites[siteID].build()
        );
    }

    public static void executeClickEvent(ItemStack is, Inventory inv, Player p){
            IPermissionPlayer permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPermissionPlayer(p.getUniqueId());
                 ICloudPlayer player = CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(p.getName());
        assert permissionPlayer != null;
        long timeoutdjaj = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(999999);

            Integer needcoinspremium7 = 5000 - BaumAPI.getCoinsAPI().getCoins(p);
            Integer needcoinspremium30 = 10000 - BaumAPI.getCoinsAPI().getCoins(p);
            Integer needcoinspremium90 =  25000 - BaumAPI.getCoinsAPI().getCoins(p);

            Integer needcoinsprime7 = 10000 - BaumAPI.getCoinsAPI().getCoins(p);
            Integer needcoinsprime30 = 25000 - BaumAPI.getCoinsAPI().getCoins(p);
            Integer needcoinsprime90 = 50000 - BaumAPI.getCoinsAPI().getCoins(p);

            Integer needcoinsbaum7 = 50000 - BaumAPI.getCoinsAPI().getCoins(p);

        if(is.getType() != Material.AIR && is.getType() != Material.STAINED_GLASS_PANE){
            p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BASS, 1, 1);
        }

        if(!inv.getTitle().contains("Shop")){
            if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§cZurück")){
                p.closeInventory();
                p.openInventory(sites[RANK_PREMIUM].build());
            }
        }

        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§cAblehnen")){
            p.closeInventory();
            p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
            p.addPotionEffect(
                    new PotionEffect(PotionEffectType.BLINDNESS, 30, 10)
            );
        }

        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§aBestätigen")){
            if(inv.getTitle().equalsIgnoreCase("Premium 7 Tage")){
                if(permissionPlayer.getPermissionGroupInfoList().size() != 1)  {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDu kannst dir kein Premium als höhrern Rang kaufen§8!");
                } else {
                    if(BaumAPI.getCoinsAPI().getCoins(p) >= 5000) {

                        permissionPlayer.getPermissionGroupInfoList().removeAll(permissionPlayer.getPermissionGroupInfoList());
                         long timeout = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(7);
                        permissionPlayer.addPermissionGroup(new PlayerPermissionGroupInfo("Premium", timeout));
                        permissionPlayer.update();
                        BaumAPI.getCoinsAPI().removeCoins(p,5000);
                        player.kick(Data.PREFIX + "§7Du hast einen neuen Rang bekommen§8!");
                    } else {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + needcoinspremium7 + ")");
                    }
                }
            }
             if(inv.getTitle().equalsIgnoreCase("Premium 30 Tage")){
                if(permissionPlayer.getPermissionGroupInfoList().size() != 1)  {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDu kannst dir kein Premium als höhrern Rang kaufen§8!");
                } else {
                    if(BaumAPI.getCoinsAPI().getCoins(p) >= 10000) {

                        permissionPlayer.getPermissionGroupInfoList().removeAll(permissionPlayer.getPermissionGroupInfoList());
                         long timeout = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(30);
                        permissionPlayer.addPermissionGroup(new PlayerPermissionGroupInfo("Premium", timeout));
                        permissionPlayer.update();
                        BaumAPI.getCoinsAPI().removeCoins(p,10000);
                        player.kick(Data.PREFIX + "§7Du hast einen neuen Rang bekommen§8!");
                    } else {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + needcoinspremium30 + ")");
                    }
                }
            }
             if(inv.getTitle().equalsIgnoreCase("Premium 90 Tage")){
                if(permissionPlayer.getPermissionGroupInfoList().size() != 1)  {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDu kannst dir kein Premium als höhrern Rang kaufen§8!");
                } else {
                    if(BaumAPI.getCoinsAPI().getCoins(p) >= 25000) {

                        permissionPlayer.getPermissionGroupInfoList().removeAll(permissionPlayer.getPermissionGroupInfoList());
                         long timeout = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(90);
                        permissionPlayer.addPermissionGroup(new PlayerPermissionGroupInfo("Premium", timeout));
                        permissionPlayer.update();
                        BaumAPI.getCoinsAPI().removeCoins(p,25000);
                        player.kick(Data.PREFIX + "§7Du hast einen neuen Rang bekommen§8!");
                    } else {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + needcoinspremium90 + ")");
                    }
                }
            }
             if(inv.getTitle().equalsIgnoreCase("Prime 7 Tage")){
                if(!permissionPlayer.hasPermissionGroup("default") || !permissionPlayer.hasPermissionGroup("Premium"))  {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDu kannst dir kein Prime als höhrern Rang kaufen§8!");
                } else {
                    if(BaumAPI.getCoinsAPI().getCoins(p) >= 10000) {

                        permissionPlayer.getPermissionGroupInfoList().removeAll(permissionPlayer.getPermissionGroupInfoList());
                         long timeout = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(7);
                        permissionPlayer.addPermissionGroup(new PlayerPermissionGroupInfo("Prime", timeout));
                        permissionPlayer.update();
                        BaumAPI.getCoinsAPI().removeCoins(p,10000);
                        player.kick(Data.PREFIX + "§7Du hast einen neuen Rang bekommen§8!");
                    } else {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + needcoinsprime7 + ")");
                    }
                }
            }
             if(inv.getTitle().equalsIgnoreCase("Prime 30 Tage")){
                if(!permissionPlayer.hasPermissionGroup("default") || !permissionPlayer.hasPermissionGroup("Premium"))  {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDu kannst dir kein Prime als höhrern Rang kaufen§8!");
                } else {
                    if(BaumAPI.getCoinsAPI().getCoins(p) >= 25000) {
                        permissionPlayer.getPermissionGroupInfoList().removeAll(permissionPlayer.getPermissionGroupInfoList());
                         long timeout = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(30);
                        permissionPlayer.addPermissionGroup(new PlayerPermissionGroupInfo("Prime", timeout));
                        permissionPlayer.update();
                        BaumAPI.getCoinsAPI().removeCoins(p,25000);
                        player.kick(Data.PREFIX + "§7Du hast einen neuen Rang bekommen§8!");
                    } else {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + needcoinsprime30 + ")");
                    }
                }
            }
            if(inv.getTitle().equalsIgnoreCase("Prime 90 Tage")){
                if(!permissionPlayer.hasPermissionGroup("default") || !permissionPlayer.hasPermissionGroup("Premium"))  {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDu kannst dir kein Prime als höhrern Rang kaufen§8!");
                } else {
                    if(BaumAPI.getCoinsAPI().getCoins(p) >= 50000) {

                        permissionPlayer.getPermissionGroupInfoList().removeAll(permissionPlayer.getPermissionGroupInfoList());
                         long timeout = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(90);
                        permissionPlayer.addPermissionGroup(new PlayerPermissionGroupInfo("Prime", timeout));
                        permissionPlayer.update();
                        BaumAPI.getCoinsAPI().removeCoins(p,50000);
                        player.kick(Data.PREFIX + "§7Du hast einen neuen Rang bekommen§8!");
                    } else {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + needcoinsprime90 + ")");
                    }
                }
            }
             if(inv.getTitle().equalsIgnoreCase("Baum 7 Tage")){
                 if(permissionPlayer.getPermissionGroupInfoList().size() != 1)  {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDu kannst dir keinen Rang mehr kaufen weil du entweder einen höheren hast oder schon einen anderen hast§8!");
                } else {

                    if(BaumAPI.getCoinsAPI().getCoins(p) >= 50000) {

                        permissionPlayer.getPermissionGroupInfoList().removeAll(permissionPlayer.getPermissionGroupInfoList());
                         long timeout = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(7);
                        permissionPlayer.addPermissionGroup(new PlayerPermissionGroupInfo("Baum", timeout));
                        permissionPlayer.update();
                        BaumAPI.getCoinsAPI().removeCoins(p,50000);
                        player.kick(Data.PREFIX + "§7Du hast einen neuen Rang bekommen§8!");
                    } else {
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + needcoinsbaum7 + ")");
                    }
                }
            }
             if(inv.getTitle().equalsIgnoreCase("Baumspende")){
                 if(BaumAPI.getCoinsAPI().getCoins(p) >= 10000){
                     SQLBaum.addBaum(p);
                     BaumAPI.getCoinsAPI().removeCoins(p, 50000);
                     p.sendMessage(Data.PREFIX + "§eDanke, das du die Umwelt unterstützt. Mach weiter so!");
                     p.sendTitle("", "§aErfolgreich Gekauft");
                     p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                 }else{
                     p.closeInventory();
                     p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                     p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + (10000 - BaumAPI.getCoinsAPI().getCoins(p)) + "§7)");
                 }
             }
            if(inv.getTitle().equalsIgnoreCase("Baumspende Größer")){
                if(BaumAPI.getCoinsAPI().getCoins(p) >= 50000){
                    SQLBaum.addBaum(p);
                    SQLBaum.addBaum(p);
                    SQLBaum.addBaum(p);
                    BaumAPI.getCoinsAPI().removeCoins(p, 50000);
                    p.sendMessage(Data.PREFIX + "§eDanke, das du die Umwelt unterstützt. Mach weiter so!");
                    p.sendTitle("", "§aErfolgreich Gekauft");
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                }else{
                    p.closeInventory();
                    p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                    p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + (50000 - BaumAPI.getCoinsAPI().getCoins(p)) + "§7)");
                }
            }
            if(inv.getTitle().equalsIgnoreCase("Angel Gadget")){
                if(BaumAPI.getCoinsAPI().getCoins(p) >= 3000){
                    if(p.hasPermission("system.bffa.gadget.angel")){
                        p.sendMessage(Data.PREFIX + "§cDu besitzt dieses Item schon!");
                        p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                        p.closeInventory();
                    }else {
                        BaumAPI.getCoinsAPI().removeCoins(p, 3000);
                        permissionPlayer.addPermission(new Permission("system.bffa.gadget.angel", timeoutdjaj, true));
                        p.sendMessage(Data.PREFIX + "§eDanke für deinen Kauf!");
                        p.sendTitle("", "§aErfolgreich Gekauft");
                    }
                }else{
                    p.closeInventory();
                    p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                    p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + (3000 - BaumAPI.getCoinsAPI().getCoins(p)) + "§7)");
                }
            }
            if(inv.getTitle().equalsIgnoreCase("Enderperle Gadget")){
                if(BaumAPI.getCoinsAPI().getCoins(p) >= 2500){
                    if(p.hasPermission("system.bffa.gadget.enderpearl")){
                        p.sendMessage(Data.PREFIX + "§cDu besitzt dieses Item schon!");
                        p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                        p.closeInventory();
                    }else {
                        BaumAPI.getCoinsAPI().removeCoins(p, 2500);
                        permissionPlayer.addPermission(new Permission("system.bffa.gadget.enderpearl", timeoutdjaj, true));
                        p.sendMessage(Data.PREFIX + "§eDanke für deinen Kauf!");
                        p.closeInventory();
                        p.sendTitle("", "§aErfolgreich Gekauft");
                    }
                }else{
                    p.closeInventory();
                    p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                    p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + (2500 - BaumAPI.getCoinsAPI().getCoins(p)) + "§7)");
                }
            }
            if(inv.getTitle().equalsIgnoreCase("Schneeball Gadget")){
                if(BaumAPI.getCoinsAPI().getCoins(p) >= 2500){
                    if(p.hasPermission("system.bffa.gadget.schneeball")){
                        p.sendMessage(Data.PREFIX + "§cDu besitzt dieses Item schon!");
                        p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                        p.closeInventory();
                    }else {
                        BaumAPI.getCoinsAPI().removeCoins(p, 2500);
                        permissionPlayer.addPermission(new Permission("system.bffa.gadget.schneeball", timeoutdjaj, true));
                        p.sendMessage(Data.PREFIX + "§eDanke für deinen Kauf!");
                        p.closeInventory();
                        p.sendTitle("", "§aErfolgreich Gekauft");
                    }
                }else{
                    p.closeInventory();
                    p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                    p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + (2500 - BaumAPI.getCoinsAPI().getCoins(p)) + "§7)");
                }
            }
            if(inv.getTitle().equalsIgnoreCase("Speed Gadget")){
                if(BaumAPI.getCoinsAPI().getCoins(p) >= 2000){
                    if(p.hasPermission("system.bffa.gadget.speed")){
                        p.sendMessage(Data.PREFIX + "§cDu besitzt dieses Item schon!");
                        p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                        p.closeInventory();
                    }else {
                        BaumAPI.getCoinsAPI().removeCoins(p, 2000);
                        permissionPlayer.addPermission(new Permission("system.bffa.gadget.speed", timeoutdjaj, true));
                        p.sendMessage(Data.PREFIX + "§eDanke für deinen Kauf!");
                        p.closeInventory();
                        p.sendTitle("", "§aErfolgreich Gekauft");
                    }
                }else{
                    p.closeInventory();
                    p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                    p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + (2000 - BaumAPI.getCoinsAPI().getCoins(p)) + "§7)");
                }
            }
            if(inv.getTitle().equalsIgnoreCase("Angreifer Kit")){
                if(BaumAPI.getCoinsAPI().getCoins(p) >= 5000){
                    if(p.hasPermission("system.bffa.kit.angreifer")){
                        p.sendMessage(Data.PREFIX + "§cDu besitzt dieses Item schon!");
                        p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                        p.closeInventory();
                    }else {
                        BaumAPI.getCoinsAPI().removeCoins(p, 5000);
                        permissionPlayer.addPermission(new Permission("system.bffa.kit.angreifer", timeoutdjaj, true));
                        p.sendMessage(Data.PREFIX + "§eDanke für deinen Kauf!");
                        p.closeInventory();
                        p.sendTitle("", "§aErfolgreich Gekauft");
                    }
                }else{
                    p.closeInventory();
                    p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                    p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + (5000 - BaumAPI.getCoinsAPI().getCoins(p)) + "§7)");
                }
            }
            if(inv.getTitle().equalsIgnoreCase("Pyro Kit")){
                if(BaumAPI.getCoinsAPI().getCoins(p) >= 3000){
                    if(p.hasPermission("system.bffa.kit.pyro")){
                        p.sendMessage(Data.PREFIX + "§cDu besitzt dieses Item schon!");
                        p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                        p.closeInventory();
                    }else {
                        BaumAPI.getCoinsAPI().removeCoins(p, 3000);
                        permissionPlayer.addPermission(new Permission("system.bffa.kit.pyro", timeoutdjaj, true));
                        p.sendMessage(Data.PREFIX + "§eDanke für deinen Kauf!");
                        p.closeInventory();
                        p.sendTitle("", "§aErfolgreich Gekauft");
                    }
                }else{
                    p.closeInventory();
                    p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                    p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + (3000 - BaumAPI.getCoinsAPI().getCoins(p)) + "§7)");
                }
            }
            if(inv.getTitle().equalsIgnoreCase("Späher Kit")){
                if(BaumAPI.getCoinsAPI().getCoins(p) >= 7500){
                    if(p.hasPermission("system.bffa.kit.späher")){
                        p.sendMessage(Data.PREFIX + "§cDu besitzt dieses Item schon!");
                        p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                        p.closeInventory();
                    }else {
                        BaumAPI.getCoinsAPI().removeCoins(p, 7500);
                        permissionPlayer.addPermission(new Permission("system.bffa.kit.späher", timeoutdjaj, true));
                        p.sendMessage(Data.PREFIX + "§eDanke für deinen Kauf!");
                        p.closeInventory();
                        p.sendTitle("", "§aErfolgreich Gekauft");
                    }
                }else{
                    p.closeInventory();
                    p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                    p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + (7500 - BaumAPI.getCoinsAPI().getCoins(p)) + "§7)");
                }
            }
            if(inv.getTitle().equalsIgnoreCase("Tank Kit")){
                if(BaumAPI.getCoinsAPI().getCoins(p) >= 5000){
                    if(p.hasPermission("system.bffa.kit.tank")){
                        p.sendMessage(Data.PREFIX + "§cDu besitzt dieses Item schon!");
                        p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                        p.closeInventory();
                    }else {
                        BaumAPI.getCoinsAPI().removeCoins(p, 5000);
                        permissionPlayer.addPermission(new Permission("system.bffa.kit.tank", timeoutdjaj, true));
                        p.sendMessage(Data.PREFIX + "§eDanke für deinen Kauf!");
                        p.closeInventory();
                        p.sendTitle("", "§aErfolgreich Gekauft");
                    }
                }else{
                    p.closeInventory();
                    p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                    p.sendMessage(Data.PREFIX + "§cDir fehlen Coins §7(" + (5000 - BaumAPI.getCoinsAPI().getCoins(p)) + "§7)");
                }
            }
            permissionPlayer.update();
        }

        /*
           ////////////////////// MENU ITEMS ////////////////////////////////
         */
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §6Premium")){
            p.openInventory(sites[RANK_PREMIUM].build());
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §bPrime")){
            p.openInventory(sites[RANK_PRIME].build());
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §2BAUM")){
            p.openInventory(sites[RANK_BAUM].build());
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §2Bäume §7'Billig'")){
            p.openInventory(sites[BAUM_CHEAP].build());
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §2Bäume §7'Teurer'")){
            p.openInventory(sites[BAUM_NONCHEAP].build());
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §eKits")){
            p.openInventory(sites[BFFA_KIT].build());
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §eGadgets")){
            p.openInventory(sites[BFFA_GADGETS].build());
        }

        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§7Ränge")){
            p.openInventory(sites[RANK_PREMIUM].build());
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§cZurück") && inv.getTitle().contains("Shop")){
            new InventoryM(p).opensettingsinv();
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§7BäumeAktion")){
            p.openInventory(sites[BAUM_CHEAP].build());
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§7BuildFFA")){
            p.openInventory(sites[BFFA_KIT].build());
        }

        /*
                ///////////////////////////////////// BUY ITEMS /////////////////////////////////////////
         */

        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§6Premium §8× §77 Tage")){
            p.openInventory(new InventorysShop().bestätInv(p, "Premium 7 Tage", sites[RANK_PREMIUM]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§6Premium §8× §730 Tage")){
            p.openInventory(new InventorysShop().bestätInv(p, "Premium 30 Tage", sites[RANK_PREMIUM]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§6Premium §8× §790 Tage")){
            p.openInventory(new InventorysShop().bestätInv(p, "Premium 90 Tage", sites[RANK_PREMIUM]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§bPrime §8× §77 Tage")){
            p.openInventory(new InventorysShop().bestätInv(p, "Prime 7 Tage", sites[RANK_PRIME]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§bPrime §8× §730 Tage")){
            p.openInventory(new InventorysShop().bestätInv(p, "Prime 30 Tage", sites[RANK_PRIME]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§bPrime §8× §790 Tage")){
            p.openInventory(new InventorysShop().bestätInv(p, "Prime 90 Tage", sites[RANK_PRIME]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§2BAUM §8× §77 Tage")){
            p.openInventory(new InventorysShop().bestätInv(p, "Baum 7 Tage", sites[RANK_BAUM]));
        }

        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§2Baumspende §8× §71 Baum")){
            p.openInventory(new InventorysShop().bestätInv(p, "Baumspende", sites[BAUM_CHEAP]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§2Baumspende §8× §71 Baum §8(§7Größerer Baum)")){
            p.openInventory(new InventorysShop().bestätInv(p, "Baumspende Größer", sites[BAUM_NONCHEAP]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §7Angel")){
            p.openInventory(new InventorysShop().bestätInv(p, "Angel Gadget", sites[BFFA_GADGETS]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §5Enderperle")){
            p.openInventory(new InventorysShop().bestätInv(p, "Enderperle Gadget", sites[BFFA_GADGETS]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §fSchneeball")){
            p.openInventory(new InventorysShop().bestätInv(p, "Schneeball Gadget", sites[BFFA_GADGETS]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §3Speed")){
            p.openInventory(new InventorysShop().bestätInv(p, "Speed Gadget", sites[BFFA_GADGETS]));
        }

        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §3Angreifer")){
            p.openInventory(new InventorysShop().bestätInv(p, "Angreifer Kit", sites[BFFA_KIT]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §cPyro")){
            p.openInventory(new InventorysShop().bestätInv(p, "Pyro Kit", sites[BFFA_KIT]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §2Späher")){
            p.openInventory(new InventorysShop().bestätInv(p, "Späher Kit", sites[BFFA_KIT]));
        }
        if(is.getItemMeta().getDisplayName().equalsIgnoreCase("§8× §5Tank")){
            p.openInventory( new InventorysShop().bestätInv(p, "Tank Kit", sites[BFFA_KIT]));
        }
    }

}
