package net.minebaum.lobbysystem.utils.shop;

import net.minebaum.baumapi.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ShopSite {

    private ItemStack[] menus;
    private ItemStack[] fieldItems;
    private String name;
    private Inventory finaleInv;
    public static final int A_MENU_ITEM = 10,
                            B_MENU_ITEM = 19,
                            C_MENU_ITEM = 28;

    public static final int FIELD_1 = 12,
                            FIELD_2 = 13,
                            FIELD_3 = 14,
                            FIELD_4 = 15,
                            FIELD_5 = 16,
                            FIELD_6 = 21,
                            FIELD_7 = 22,
                            FIELD_8 = 23,
                            FIELD_9 = 24,
                            FIELD_10 = 25,
                            FIELD_11 = 30,
                            FIELD_12 = 31,
                            FIELD_13 = 32,
                            FIELD_14 = 33,
                            FIELD_15 = 34;
    public static final int A_MAIN_MENU_ITEM = (9*6)-4;
    public static final int B_MAIN_MENU_ITEM = A_MAIN_MENU_ITEM-1;
    public static final int C_MAIN_MENU_ITEM = B_MAIN_MENU_ITEM-1;

    private int CURRENT_MENU_ITEM;
    private int CURRENT_MAIN_MENU_ITEM;

    public ShopSite(String siteName, int CURRENT_MENU_ITEM, int CURRENT_MAIN_MENU_ITEM){
        menus = new ItemStack[29];
        fieldItems = new ItemStack[35];
        this.CURRENT_MAIN_MENU_ITEM = CURRENT_MAIN_MENU_ITEM;
        this.CURRENT_MENU_ITEM = CURRENT_MENU_ITEM;
        ItemStack air = new ItemBuilder(Material.AIR, 1, (short) 0).build();
        menus[A_MENU_ITEM] = air;
        menus[B_MENU_ITEM] = air;
        menus[C_MENU_ITEM] = air;
        fieldItems[FIELD_1] = air;
        fieldItems[FIELD_2] = air;
        fieldItems[FIELD_3] = air;
        fieldItems[FIELD_4] = air;
        fieldItems[FIELD_5] = air;
        fieldItems[FIELD_6] = air;
        fieldItems[FIELD_7] = air;
        fieldItems[FIELD_8] = air;
        fieldItems[FIELD_9] = air;
        fieldItems[FIELD_10] = air;
        fieldItems[FIELD_11] = air;
        fieldItems[FIELD_12] = air;
        fieldItems[FIELD_13] = air;
        fieldItems[FIELD_14] = air;
        fieldItems[FIELD_15] = air;
        finaleInv = new InventorysShop().setstandartShop();
        name = siteName;
    }

    public ShopSite setupMenu(ItemStack a, ItemStack b, ItemStack c){
        menus[A_MENU_ITEM] = a;
        menus[B_MENU_ITEM] = b;
        menus[C_MENU_ITEM] = c;
        return this;
    }

    public ShopSite setupFieldItems(ItemStack[] fieldItems){
        this.fieldItems = fieldItems;
        return this;
    }

    public void addFIeldItem(int slotType, ItemStack item){
        fieldItems[slotType] = item;
    }

    public void addMenuItem(int slotType, ItemStack item){
        fieldItems[slotType] = item;
    }

    public ItemStack[] getFieldItems() {
        return fieldItems;
    }

    public ItemStack[] getMenuItems(){
        return menus;
    }

    public ItemStack getMenuItem(int slotType){
        return menus[slotType];
    }

    public ItemStack getFieldItem(int slotType){
        return fieldItems[slotType];
    }

    public Inventory build(){
        Inventory inv = finaleInv;
        inv.setItem(A_MENU_ITEM, menus[A_MENU_ITEM]);
        inv.setItem(B_MENU_ITEM, menus[B_MENU_ITEM]);
        inv.setItem(C_MENU_ITEM, menus[C_MENU_ITEM]);
        inv.setItem(FIELD_1, fieldItems[FIELD_1]);
        inv.setItem(FIELD_2, fieldItems[FIELD_2]);
        inv.setItem(FIELD_3, fieldItems[FIELD_3]);
        inv.setItem(FIELD_4, fieldItems[FIELD_4]);
        inv.setItem(FIELD_5, fieldItems[FIELD_5]);
        inv.setItem(FIELD_6, fieldItems[FIELD_6]);
        inv.setItem(FIELD_7, fieldItems[FIELD_7]);
        inv.setItem(FIELD_8, fieldItems[FIELD_8]);
        inv.setItem(FIELD_9, fieldItems[FIELD_9]);
        inv.setItem(FIELD_10, fieldItems[FIELD_10]);
        inv.setItem(FIELD_11, fieldItems[FIELD_11]);
        inv.setItem(FIELD_12, fieldItems[FIELD_12]);
        inv.setItem(FIELD_13, fieldItems[FIELD_13]);
        inv.setItem(FIELD_14, fieldItems[FIELD_14]);
        inv.setItem(FIELD_15, fieldItems[FIELD_15]);
        return inv;
    }

}
