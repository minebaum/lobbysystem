package net.minebaum.lobbysystem.utils.shop;

import net.minebaum.baumapi.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class ShopData {

    public static ArrayList<String> PREMIUM_MENU_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> PRIME_MENU_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> BAUM_MENU_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> BAUM_CHEAP_MENU_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> BAUM_NORM_MENU_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> PREMIUM_7_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> PREMIUM_30_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> PREMIUM_90_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> PRIME_7_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> PRIME_30_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> PRIME_90_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> BAUM_7_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> ENDERPEARL_GADGET_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> SCHENNBALL_GADGET_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> ANGEL_GADGET_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> SPEED_GADGET_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> TANK_KIT_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> ANGREIFER_KIT_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> PYRO_KIT_ITEM_LORE = new ArrayList<>();
    public static ArrayList<String> SPÄHER_KIT_ITEM_LORE = new ArrayList<>();


    public static ItemStack PREMIUM_MENU_ITEM;
    public static ItemStack PRIME_MENU_ITEM;
    public static ItemStack BAUM_MENU_ITEM;
    public static ItemStack BAUM_CHEAP_MENU_ITEM;
    public static ItemStack BAUM_NORM_MENU_ITEM;
    public static ItemStack BFFA_KIT_MENU_ITEM;
    public static ItemStack BFFA_GADGET_MENU_ITEM;

    //PREMIUM FIELD ITEMS
    public static ItemStack PREMIUM_30_ITEM;
    public static ItemStack PREMIUM_7_ITEM;
    public static ItemStack PREMIUM_90_ITEM;
    //PRIME FIELD ITEMS
    public static ItemStack PRIME_30_ITEM;
    public static ItemStack PRIME_7_ITEM;
    public static ItemStack PRIME_90_ITEM;
    //BAUM FIELD ITEMS
    public static ItemStack BAUM_7_ITEM;
    //BAUM CHEAP FIELD ITEM
    public static ItemStack BAUM_CHEAP_FIELD_ITEM;
    //BAUM NORM FIELD ITEM
    public static ItemStack BAUM_NORM_FIELD_ITEM;
    //PREMIUM FIELD ITEMS
    public static ItemStack ENDERPERLE_GADGET_ITEM;
    public static ItemStack SCHNEEBALL_GADGET_ITEM;
    public static ItemStack ANGEL_GADGET_ITEM;
    public static ItemStack SPEED_GADGET_ITEM;
    //PREMIUM FIELD ITEMS
    public static ItemStack TANK_KIT_ITEM;
    public static ItemStack ANGREIFER_KIT_ITEM;
    public static ItemStack PYRO_KIT_ITEM;
    public static ItemStack SPÄHER_KIT_ITEM;

    public static void setup(){
        PREMIUM_MENU_ITEM_LORE.add("§8§m---------------------------------");
        PREMIUM_MENU_ITEM_LORE.add("§eRechte §8>>");
        PREMIUM_MENU_ITEM_LORE.add("§7  - 'Premium' Prefix");
        PREMIUM_MENU_ITEM_LORE.add("§7  - Doppel Sprung in der Lobby");
        PREMIUM_MENU_ITEM_LORE.add("§7  - Goldene Schuhe");
        PREMIUM_MENU_ITEM_LORE.add("§7  - Volle Server betreten.");
        PREMIUM_MENU_ITEM_LORE.add("§7  - Eine größere Tägliche belohnung.");
        PREMIUM_MENU_ITEM_LORE.add("§8§m------------");
        PRIME_MENU_ITEM_LORE.add("§8§m---------------------------------");
        PRIME_MENU_ITEM_LORE.add("§eRechte §8>>");
        PRIME_MENU_ITEM_LORE.add("§7  - 'Prime' Prefix");
        PRIME_MENU_ITEM_LORE.add("§7  - Alle Rechte des Premium Ranges");
        PRIME_MENU_ITEM_LORE.add("§7  - Hellblaue Schuhe");
        PRIME_MENU_ITEM_LORE.add("§7  - Farbig schreiben");
        PRIME_MENU_ITEM_LORE.add("§7  - Das Speed Gadget auf BuildFFA");
        PRIME_MENU_ITEM_LORE.add("§8§m---------------------------------");
        BAUM_MENU_ITEM_LORE.add("§8§m---------------------------------");
        BAUM_MENU_ITEM_LORE.add("§eRechte §8>>");
        BAUM_MENU_ITEM_LORE.add("§7  - 'BAUM' Prefix");
        BAUM_MENU_ITEM_LORE.add("§7  - Alle Rechte des Prime Ranges");
        BAUM_MENU_ITEM_LORE.add("§7  - Grüne Schuhe");
        BAUM_MENU_ITEM_LORE.add("§7  - Hervorgehobene Nachrichten");
        BAUM_MENU_ITEM_LORE.add("§7  - Eine größere tägliche Belohnung");
        BAUM_MENU_ITEM_LORE.add("§8§m---------------------------------");
        BAUM_CHEAP_MENU_ITEM_LORE.add("§8§m-----------------------------------------");
        BAUM_CHEAP_MENU_ITEM_LORE.add("§7Spende einen Baum an eine Hilforganisation.");
        BAUM_CHEAP_MENU_ITEM_LORE.add("§8§m-----------------------------------------");
        BAUM_NORM_MENU_ITEM_LORE.add("§8§m-----------------------------------------");
        BAUM_NORM_MENU_ITEM_LORE.add("§7Spende einen Baum an eine Hilforganisation.");
        BAUM_NORM_MENU_ITEM_LORE.add("§8§m-----------------------------------------");
        PREMIUM_7_ITEM_LORE.add("§7Preis §8>> §25.000 Bäume");
        PREMIUM_30_ITEM_LORE.add("§7Preis §8>> §210.000 Bäume");
        PREMIUM_90_ITEM_LORE.add("§7Preis §8>> §225.000 Bäume");
        PRIME_7_ITEM_LORE.add("§7Preis §8>> §210.000 Bäume");
        PRIME_30_ITEM_LORE.add("§7Preis §8>> §225.000 Bäume");
        PRIME_90_ITEM_LORE.add("§7Preis §8>> §250.000 Bäume");
        BAUM_7_ITEM_LORE.add("§7Preis §8>> §250.000 Bäume");
        ENDERPEARL_GADGET_ITEM_LORE.add("§7Preis §8>> §22.500 Bäume");
        ANGEL_GADGET_ITEM_LORE.add("§7Preis §8>> §23.000 Bäume");
        SCHENNBALL_GADGET_ITEM_LORE.add("§7Preis §8>> §22.500 Bäume");
        SPEED_GADGET_ITEM_LORE.add("§7Preis §8>> §22.000 Bäume");
        TANK_KIT_ITEM_LORE.add("§7Preis §8>> §25.000 Bäume");
        ANGREIFER_KIT_ITEM_LORE.add("§7Preis §8>> §25.000 Bäume");
        SPÄHER_KIT_ITEM_LORE.add("§7Preis §8>> §27.500 Bäume");
        PYRO_KIT_ITEM_LORE.add("§7Preis §8>> §23.000 Bäume");
        SPÄHER_KIT_ITEM = new ItemBuilder(Material.BOW, 1, (short) 0)
            .setDisplayname("§8× §2Späher").setLore(SPÄHER_KIT_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        PYRO_KIT_ITEM = new ItemBuilder(Material.FLINT_AND_STEEL, 1, (short) 0)
            .setDisplayname("§8× §cPyro").setLore(PYRO_KIT_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        ANGREIFER_KIT_ITEM = new ItemBuilder(Material.DIAMOND_SWORD, 1, (short) 0)
            .setDisplayname("§8× §3Angreifer").setLore(ANGREIFER_KIT_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        TANK_KIT_ITEM = new ItemBuilder(Material.IRON_CHESTPLATE, 1, (short) 0)
            .setDisplayname("§8× §5Tank").setLore(TANK_KIT_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        SPEED_GADGET_ITEM = new ItemBuilder(Material.FEATHER, 1, (short) 0)
            .setDisplayname("§8× §3Speed").setLore(SPEED_GADGET_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        ANGEL_GADGET_ITEM = new ItemBuilder(Material.FISHING_ROD, 1, (short) 0)
            .setDisplayname("§8× §7Angel").setLore(ANGEL_GADGET_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        SCHNEEBALL_GADGET_ITEM = new ItemBuilder(Material.SNOW_BALL, 1, (short) 0)
            .setDisplayname("§8× §fSchneeball").setLore(SCHENNBALL_GADGET_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        ENDERPERLE_GADGET_ITEM = new ItemBuilder(Material.ENDER_PEARL, 1, (short) 0)
            .setDisplayname("§8× §5Enderperle").setLore(ENDERPEARL_GADGET_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        BAUM_NORM_FIELD_ITEM = new ItemBuilder(Material.SAPLING, 1, (short) 5)
            .setDisplayname("§2Baumspende §8× §71 Baum §8(§7Größerer Baum)").setLore(BAUM_7_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        BAUM_CHEAP_FIELD_ITEM = new ItemBuilder(Material.SAPLING, 1, (short) 5)
            .setDisplayname("§2Baumspende §8× §71 Baum").setLore(PRIME_7_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        BAUM_7_ITEM = new ItemBuilder(Material.SAPLING, 1, (short) 5)
            .setDisplayname("§2BAUM §8× §77 Tage").setLore(BAUM_7_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        PRIME_90_ITEM = new ItemBuilder(Material.DIAMOND_BLOCK, 1, (short) 0)
            .setDisplayname("§bPrime §8× §790 Tage").setLore(PRIME_90_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        PRIME_7_ITEM = new ItemBuilder(Material.DIAMOND, 1, (short) 0)
            .setDisplayname("§bPrime §8× §77 Tage").setLore(PRIME_7_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        PRIME_30_ITEM = new ItemBuilder(Material.DIAMOND_ORE, 1, (short) 0)
            .setDisplayname("§bPrime §8× §730 Tage").setLore(PRIME_30_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        PREMIUM_90_ITEM = new ItemBuilder(Material.GOLD_BLOCK, 1, (short) 0)
            .setDisplayname("§6Premium §8× §790 Tage").setLore(PREMIUM_90_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        PREMIUM_7_ITEM = new ItemBuilder(Material.GOLD_INGOT, 1, (short) 0)
            .setDisplayname("§6Premium §8× §77 Tage").setLore(PREMIUM_7_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        PREMIUM_30_ITEM = new ItemBuilder(Material.GOLD_ORE, 1, (short) 0)
            .setDisplayname("§6Premium §8× §730 Tage").setLore(PREMIUM_30_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        PREMIUM_MENU_ITEM = new ItemBuilder(Material.GOLD_INGOT, 1, (short) 0)
                .setDisplayname("§8× §6Premium").setLore(PREMIUM_MENU_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        PRIME_MENU_ITEM = new ItemBuilder(Material.DIAMOND, 1, (short) 0)
                .setDisplayname("§8× §bPrime").setLore(PRIME_MENU_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        BAUM_MENU_ITEM = new ItemBuilder(Material.SAPLING, 1, (short) 5)
                .setDisplayname("§8× §2BAUM").setLore(BAUM_MENU_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        BAUM_CHEAP_MENU_ITEM = new ItemBuilder(Material.SAPLING, 1, (short) 5)
                .setDisplayname("§8× §2Bäume §7'Billig'").setLore(BAUM_CHEAP_MENU_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        BAUM_NORM_MENU_ITEM = new ItemBuilder(Material.SAPLING, 1, (short) 5)
                .setDisplayname("§8× §2Bäume §7'Teurer'").setLore(BAUM_NORM_MENU_ITEM_LORE).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        BFFA_KIT_MENU_ITEM = new ItemBuilder(Material.SAPLING, 1, (short) 0)
                .setDisplayname("§8× §eKits").addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
        BFFA_GADGET_MENU_ITEM = new ItemBuilder(Material.ENDER_PEARL, 1, (short) 0)
                .setDisplayname("§8× §eGadgets").addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build();
    }

}