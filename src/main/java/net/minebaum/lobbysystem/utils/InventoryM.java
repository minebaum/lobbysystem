package net.minebaum.lobbysystem.utils;

import net.minebaum.baumapi.api.GuiAPI;
import net.minebaum.baumapi.utils.Data;
import net.minebaum.baumapi.utils.ItemBuilder;
import net.minebaum.lobbysystem.mysql.SQLFly;
import net.minebaum.lobbysystem.mysql.SQLSpielerverstecker;
import net.minebaum.lobbysystem.mysql.SQLTeleport;
import net.minebaum.lobbysystem.mysql.SQLTon;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class InventoryM {

    private Player p;
    public InventoryM (Player p) {
        this.p = p;
    }
    private  Inventory inv = new GuiAPI().fillerGUI(9*3, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,(short) 15).setDisplayname(" ").build(),Data.PREFIX + "§eEinstellungen");
    private Inventory lobbysetting = new GuiAPI().fillerGUI(9, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,(short) 15).setDisplayname(" ").build(),Data.PREFIX + "§cLobby-Einstellungen");

    public Inventory getLobbysetting() {
        return lobbysetting;
    }

    public Inventory getInv() {
        return inv;
    }
    public void onsetInv() {
        Shooes.setShoes(p);
        if(p.hasPermission("system.fly")) {
            if(SQLFly.getState(p.getUniqueId().toString()) == 1) {
                p.getInventory().setItem(22,new ItemBuilder(Material.FEATHER,1 , (short)0).setDisplayname("§7FLY §8× §cOFF").build());
            } else if(SQLFly.getState(p.getUniqueId().toString()) == 2) {
                p.getInventory().setItem(22,new ItemBuilder(Material.FEATHER,1 , (short)0).setDisplayname("§7FLY §8× §aON").build());
            }
        }
        p.getInventory().setItem(2, new ItemBuilder(Material.REDSTONE_COMPARATOR, 1, (short)0).setDisplayname(Data.PREFIX +"§eEinstellungen").build());
        p.getInventory().setItem(4, new ItemBuilder(Material.COMPASS, 1, (short)0).setDisplayname(Data.PREFIX +"§cTeleporter").build());
        p.getInventory().setItem(6, new ItemBuilder(Material.RECORD_7, 1, (short)0).setDisplayname(Data.PREFIX + "§eLobby").build());
    }
    public void opensettingsinv() {
        p.playSound(p.getLocation(), Sound.ENTITY_BAT_TAKEOFF, 1 ,5);
        inv.setItem(16, new ItemBuilder(Material.GOLD_NUGGET, 1 , (short) 0).setDisplayname("§e§lShop").build());
        inv.setItem(10,new ItemBuilder(Material.REDSTONE_TORCH_ON, 1 , (short)0).setDisplayname("§c§lLobby Einstellungen").build());
        if(SQLSpielerverstecker.getState(p.getUniqueId().toString()) == 1) {
            inv.setItem(13, new ItemBuilder(Material.SPECKLED_MELON, 1, (short)0).setDisplayname("§cVerstecker §7× §aALLE").build());
        } else if(SQLSpielerverstecker.getState(p.getUniqueId().toString()) == 2) {
            inv.setItem(13, new ItemBuilder(Material.REDSTONE, 1, (short)0).setDisplayname("§cVerstecker §7× §5TEAM").build());
        } if(SQLSpielerverstecker.getState(p.getUniqueId().toString()) == 3) {
            inv.setItem(13, new ItemBuilder(Material.CLAY_BALL, 1, (short)0).setDisplayname("§cVerstecker §7× §7NIEMAND").build());
        }
        p.openInventory(inv);
    }
    public void openlobbysetting() {
        if(SQLTon.getState(p.getUniqueId().toString()) == 1) {
            p.playSound(p.getLocation(),Sound.ENTITY_BAT_TAKEOFF, 10, 2);
            lobbysetting.setItem(2, new ItemBuilder(Material.STONE_BUTTON, 1, (short) 0).setDisplayname("§cHotbar §8» §aAN").build());
        } else if(SQLTon.getState(p.getUniqueId().toString()) == 2) {
            lobbysetting.setItem(2, new ItemBuilder(Material.WOOD_BUTTON, 1, (short) 0).setDisplayname("§cHotbar §8» §cAUS").build());

        }
        if(SQLTeleport.getState(p.getUniqueId().toString()) == 1) {
            p.playSound(p.getLocation(),Sound.ENTITY_BAT_TAKEOFF, 10, 2);
            lobbysetting.setItem(6, new ItemBuilder(Material.ENDER_PEARL, 1, (short) 0).setDisplayname("§5Animation §8» §aAN").build());
        } else if(SQLTeleport.getState(p.getUniqueId().toString()) == 2) {
            lobbysetting.setItem(6, new ItemBuilder(Material.ENDER_PEARL, 1, (short) 0).setDisplayname("§5Animation §8» §cAUS").build());

        }
       p.openInventory(lobbysetting);
    }
    
}
