package net.minebaum.lobbysystem;


import eu.thesimplecloud.api.CloudAPI;
import eu.thesimplecloud.api.service.ICloudService;
import net.minebaum.baumapi.api.ActionbarAPI;
import net.minebaum.baumapi.mysql.MySQLConnector;
import net.minebaum.baumapi.utils.ItemBuilder;
import net.minebaum.baumapi.utils.Skull.Skull;
import net.minebaum.lobbysystem.commands.COMMAND_Build;
import net.minebaum.lobbysystem.commands.COMMAND_Onlinetime;
import net.minebaum.lobbysystem.commands.COMMAND_Setup;
import net.minebaum.lobbysystem.listeners.*;
import net.minebaum.lobbysystem.mysql.MySQL;
import net.minebaum.lobbysystem.utils.LocationManager;
import net.minebaum.lobbysystem.utils.TopStatsArmorstand;
import net.minebaum.lobbysystem.utils.shop.ShopData;
import net.minebaum.lobbysystem.utils.shop.ShopManager;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.EulerAngle;

public class LobbySystem extends JavaPlugin {

    public static LobbySystem instance;
    public static MySQL mySQL;
    static BukkitTask task;
    public static ArmorStand buildarmor;
    public static Villager dailyvillager;
    public static ArmorStand citybuildarmor;
    public static ArmorStand gungamearmor;
    public static ArmorStand mlgarmor;
    public static int werbung = 0;
    public static MySQLConnector gungameStats;
    public static MySQLConnector bffaStats;

    @Override
    public void onEnable() {
        instance = this;
        register();
        gungameStats = new MySQLConnector("web7447.cweb03.gamingweb.de", 3306, "gungamedb", "mok1awdawdiugh", "Ce6xNmK1O1theJAk");
        gungameStats.connect();
        bffaStats = new MySQLConnector("web7447.cweb03.gamingweb.de", 3306, "buildffastats", "mok1382", "Ce6xNmK1O1theJAk");
        bffaStats.connect();
        mySQL = new MySQL("web7447.cweb03.gamingweb.de","lobbysystem","mok13","76~xFfa2");
        mySQL.update("CREATE TABLE IF NOT EXISTS Ton(UUID varchar(65), STATE int);");
        mySQL.update("CREATE TABLE IF NOT EXISTS FLY(UUID varchar(65), STATE int);");
        mySQL.update("CREATE TABLE IF NOT EXISTS Teleport(UUID varchar(65), STATE int);");
        mySQL.update("CREATE TABLE IF NOT EXISTS Baum(Name TEXT);");
        mySQL.update("CREATE TABLE IF NOT EXISTS Spielerverstecker(UUID varchar(65), STATE int);");
        mySQL.update("CREATE TABLE IF NOT EXISTS RewardTime(UUID varchar(65), RewardTimesSpieler BIGINT,RewardTimesPremium BIGINT, RewardTimesBaum BIGINT);");
        setArmorstandsMobs();

        Bukkit.getScheduler().runTaskTimer(this, ()-> {
        TopStatsArmorstand.setTop3armorstands();
        },0,20*60*5);
        ShopData.setup();
        ShopManager.setupAllSites();
        Bukkit.getScheduler().runTaskTimer(this, new Runnable() {
            @Override
            public void run() {
                if(werbung == 6){
                    werbung = 0;
                }
                for(Player all : Bukkit.getOnlinePlayers()){
                    new ActionbarAPI(news(werbung), all).send();
                }
                werbung++;
            }
        }, 0, 40);
    }

    @Override
    public void onDisable() {

    }

    private  void register() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new JoinListener(),this);
        pm.registerEvents(new QuitListener(),this);
        pm.registerEvents(new WeatherChangeListener(),this);
        pm.registerEvents(new EntitySpawnListener(),this);
        pm.registerEvents(new EntityDamageListener(),this);
        pm.registerEvents(new COMMAND_Build(), this);
        pm.registerEvents(new PlayerItemHeldListener(), this);
        pm.registerEvents(new PlayerItemSwapListener(), this);
        pm.registerEvents(new FoodChangeListener(), this);
        pm.registerEvents(new PlayerInteractListener(), this);
        pm.registerEvents(new PlayerCloseInvenotryListener(), this);
        pm.registerEvents(new InvenotryClickListener(), this);
        pm.registerEvents(new PlayerMoveListener(), this);
        pm.registerEvents(new PlayerInteractEntityListener(), this);
        pm.registerEvents(new ToggleFlyListener(), this);

        getCommand("build").setExecutor(new COMMAND_Build());
        getCommand("setup").setExecutor(new COMMAND_Setup());
        getCommand("onlinetime").setExecutor(new COMMAND_Onlinetime());
    }
    public static void setArmorstandsMobs() {
        ICloudService iCloudService = CloudAPI.getInstance().getCloudServiceManager().getCloudServiceByName("BuildFFA-1");
        Location locarmor = new Location(Bukkit.getWorld("world"), 56.487,32,19.448,44,3);
        if(LocationManager.file.exists()) {
            buildarmor = (ArmorStand) locarmor.getWorld().spawnEntity(LocationManager.getLocation("buildffa"), EntityType.ARMOR_STAND);
        } else {
            buildarmor = (ArmorStand) locarmor.getWorld().spawnEntity(locarmor, EntityType.ARMOR_STAND);
        }
        buildarmor.setVisible(true);
        buildarmor.setGravity(false);
        buildarmor.setCustomNameVisible(true);
        buildarmor.setHelmet(Skull.getCustomSkull("http://textures.minecraft.net/texture/393309130ff12d8e757479602c3777353f22559e9fcb1b6b88dfb254ec8518be"));
        buildarmor.setChestplate(new ItemBuilder(Material.LEATHER_CHESTPLATE, 1, (short)0).build());
        buildarmor.setLeggings(new ItemBuilder(Material.GOLD_LEGGINGS, 1, (short)0).build());
        buildarmor.setBoots(new ItemBuilder(Material.DIAMOND_BOOTS, 1, (short)0).build());
        buildarmor.setArms(true);
        buildarmor.setItemInHand(new ItemBuilder(Material.SANDSTONE, 1 , (short)0).build());
        buildarmor.setLeftArmPose(new EulerAngle(Math.toRadians(51), Math.toRadians(100),Math.toRadians(0)));
        buildarmor.setRightArmPose(new EulerAngle(Math.toRadians(120), Math.toRadians(245),Math.toRadians(0)));
        buildarmor.setRightLegPose(new EulerAngle(Math.toRadians(44), Math.toRadians(223),Math.toRadians(0)));
        buildarmor.setLeftLegPose(new EulerAngle(Math.toRadians(25), Math.toRadians(12),Math.toRadians(0)));
        buildarmor.setBasePlate(false);
        assert iCloudService != null;
        buildarmor.setCustomName("§c§lMineBaum§8 ● §eBuildFFA");
        //Aormor

        //Villager
        Location locvillager = new Location(Bukkit.getWorld("world"),59.466,22,-3.452,39,-11);
        if(LocationManager.file.exists()) {
            dailyvillager = (Villager) locvillager.getWorld().spawnEntity(LocationManager.getLocation("villager"), EntityType.VILLAGER);
        } else {
            dailyvillager = (Villager) locvillager.getWorld().spawnEntity(locvillager, EntityType.VILLAGER);
        }
        dailyvillager.setCustomName("§aTägliche Belohnung");
        dailyvillager.setAI(false);
        dailyvillager.setGravity(true);
        //Villager
        //CityBuild

        //CityBuild
        //Armorstandgungame
        ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
        LeatherArmorMeta chestmeta =  (LeatherArmorMeta) chestplate.getItemMeta();
        chestmeta.setColor(Color.RED);
        chestplate.setItemMeta(chestmeta);
        ItemStack leggins = new ItemStack(Material.LEATHER_LEGGINGS);
        LeatherArmorMeta legginsmeta =  (LeatherArmorMeta) leggins.getItemMeta();
        legginsmeta.setColor(Color.RED);
        leggins.setItemMeta(legginsmeta);
        ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
        LeatherArmorMeta bootsmeta =  (LeatherArmorMeta) boots.getItemMeta();
        bootsmeta.setColor(Color.RED);
        boots.setItemMeta(bootsmeta);

        Location locarmorg = new Location(Bukkit.getWorld("world"), 56.487,32,19.448,44,3);
        if(LocationManager.file.exists()) {
            gungamearmor = (ArmorStand) locarmorg.getWorld().spawnEntity(LocationManager.getLocation("gungame"), EntityType.ARMOR_STAND);
        } else {
            gungamearmor = (ArmorStand) locarmorg.getWorld().spawnEntity(locarmor, EntityType.ARMOR_STAND);
        }
        gungamearmor.setVisible(true);
        gungamearmor.setGravity(false);
        gungamearmor.setCustomNameVisible(true);
        gungamearmor.setHelmet(Skull.getCustomSkull("http://textures.minecraft.net/texture/393309130ff12d8e757479602c3777353f22559e9fcb1b6b88dfb254ec8518be"));
        gungamearmor.setChestplate(chestplate);
        gungamearmor.setLeggings(leggins);
        gungamearmor.setBoots(boots);
        gungamearmor.setArms(true);
        gungamearmor.setItemInHand(new ItemBuilder(Material.WOOD_AXE, 1 , (short)0).build());
        gungamearmor.setLeftArmPose(new EulerAngle(Math.toRadians(51), Math.toRadians(100),Math.toRadians(0)));
        gungamearmor.setRightArmPose(new EulerAngle(Math.toRadians(120), Math.toRadians(245),Math.toRadians(0)));
        gungamearmor.setRightLegPose(new EulerAngle(Math.toRadians(327), Math.toRadians(58),Math.toRadians(14)));
        gungamearmor.setLeftLegPose(new EulerAngle(Math.toRadians(52), Math.toRadians(111),Math.toRadians(0)));
        gungamearmor.setBasePlate(false);
        assert iCloudService != null;
        gungamearmor.setCustomName("§c§lMineBaum§8 ● §eGunGame");

        Location locarmorm = new Location(Bukkit.getWorld("world"), 56.487,32,19.448,44,3);
        if(LocationManager.file.exists()) {
            mlgarmor = (ArmorStand) locarmorm.getWorld().spawnEntity(LocationManager.getLocation("mlgrush"), EntityType.ARMOR_STAND);
        } else {
            mlgarmor = (ArmorStand) locarmorm.getWorld().spawnEntity(locarmor, EntityType.ARMOR_STAND);
        }
        mlgarmor.setVisible(true);
        mlgarmor.setGravity(false);
        mlgarmor.setCustomNameVisible(true);
        mlgarmor.setHelmet(Skull.getPlayerSkull("xPxrry"));
        mlgarmor.setChestplate(chestplate);
        mlgarmor.setLeggings(leggins);
        mlgarmor.setBoots(boots);
        mlgarmor.setArms(true);
        mlgarmor.setItemInHand(new ItemBuilder(Material.STICK, 1 , (short)0).addEnchantment(Enchantment.DURABILITY, 1).build());
        mlgarmor.setLeftArmPose(new EulerAngle(Math.toRadians(51), Math.toRadians(100),Math.toRadians(0)));
        mlgarmor.setRightArmPose(new EulerAngle(Math.toRadians(120), Math.toRadians(245),Math.toRadians(0)));
        mlgarmor.setRightLegPose(new EulerAngle(Math.toRadians(327), Math.toRadians(58),Math.toRadians(14)));
        mlgarmor.setLeftLegPose(new EulerAngle(Math.toRadians(52), Math.toRadians(111),Math.toRadians(0)));
        mlgarmor.setBasePlate(false);
        assert iCloudService != null;
        mlgarmor.setCustomName("§c§lMineBaum§8 ● §eMLGRush");

        Location locarmorc = new Location(Bukkit.getWorld("world"), 56.487,32,19.448,44,3);
        if(LocationManager.file.exists()) {
            citybuildarmor = (ArmorStand) locarmorg.getWorld().spawnEntity(LocationManager.getLocation("citybuild"), EntityType.ARMOR_STAND);
        } else {
            citybuildarmor = (ArmorStand) locarmorg.getWorld().spawnEntity(locarmor, EntityType.ARMOR_STAND);
        }
        citybuildarmor.setVisible(true);
        citybuildarmor.setGravity(false);
        citybuildarmor.setCustomNameVisible(true);
        citybuildarmor.setHelmet(Skull.getPlayerSkull("Pahuul"));
        citybuildarmor.setChestplate(new ItemBuilder(Material.GOLD_CHESTPLATE, 1, (short)0).build());
        citybuildarmor.setLeggings(new ItemBuilder(Material.IRON_LEGGINGS, 1, (short)0).build());
        citybuildarmor.setBoots(new ItemBuilder(Material.LEATHER_BOOTS, 1, (short)0).build());
        citybuildarmor.setArms(true);
        citybuildarmor.setItemInHand(new ItemBuilder(Material.DIAMOND_PICKAXE, 1 , (short)0).build());
        citybuildarmor.setLeftArmPose(new EulerAngle(Math.toRadians(51), Math.toRadians(100),Math.toRadians(0)));
        citybuildarmor.setRightArmPose(new EulerAngle(Math.toRadians(120), Math.toRadians(245),Math.toRadians(0)));
        citybuildarmor.setBasePlate(false);
        assert iCloudService != null;
        citybuildarmor.setCustomName("§c§lMineBaum§8 ● §eCityBuild");

    }

    private String news(int count){
        if(count == 0){
            return "§8» §7Teamspeak gefällig? §8-> §e/ts §8«";
        }else if(count == 1){
            return "§8» §7Discord gefällig? §8-> §e/dc §8«";
        }else if(count == 2){
            return "§8» §cDu hast einen Hacker gefunden? §8-> §e/report §8«";
        }else if(count == 3){
            return "§8» §3Du brauchst Hilfe? §8-> §e/support §8«";
        }else if(count == 4){
            return "§8» §7Besuche und doch auf Twitter. §8-> §e@minebaumnet §8«";
        }else{
            return "§8» §7Tue etwas gutes für die Umwelt, kaufe Bäume im Shop. §8«";
        }
    }

}
