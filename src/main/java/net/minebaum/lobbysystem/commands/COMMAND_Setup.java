package net.minebaum.lobbysystem.commands;

import net.minebaum.baumapi.utils.Data;
import net.minebaum.lobbysystem.utils.LocationManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.Locale;

public class COMMAND_Setup implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
        Player p = (Player) s;
        if(p.hasPermission("system.setup")) {
            if(args.length == 0) {
                p.sendMessage(Data.PREFIX + "§7/setup setspawn");
                p.sendMessage(Data.PREFIX + "§7/setup setbuildffa");
                p.sendMessage(Data.PREFIX + "§7/setup setgungame");
                p.sendMessage(Data.PREFIX + "§7/setup setvillager");
                p.sendMessage(Data.PREFIX + "§7/setup setcitybuild");
                p.sendMessage(Data.PREFIX + "§7/setup setgungameteleport");
                p.sendMessage(Data.PREFIX + "§7/setup setcitybuildteleport");
                p.sendMessage(Data.PREFIX + "§7/setup setbuildffateleport");
                p.sendMessage(Data.PREFIX + "§7/setup setloc §8<§72§8>");
            } else if(args.length == 1) {
                try {
                    LocationManager.setLocation(args[0].toLowerCase() , p.getLocation());
                    p.sendMessage(Data.PREFIX  + "§7Der Loc " + args[0].toLowerCase()  + "§7 wurde gesetzt§8.");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if(args.length == 2) {
                try{
                    LocationManager.setLocation(args[1],p.getLocation());
                    p.sendMessage(Data.PREFIX + "§aDu hadt erfolgreich den Loc "+ args[1]+ " gesetzt§8.");
                }catch (NumberFormatException | IOException e) {
                    p.sendMessage(Data.PREFIX + "§cBitte gebe eine Zahl ein§8.");
                }


            } else {
                p.sendMessage(Data.PREFIX + "§7/setup setspawn");
                p.sendMessage(Data.PREFIX + "§7/setup setloc §8<§71 2 3§8>");
            }

        } else {
            p.sendMessage(Data.NOPERMS);
        }
        return false;
    }
}
