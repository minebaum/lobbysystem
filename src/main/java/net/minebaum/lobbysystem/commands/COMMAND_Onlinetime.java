package net.minebaum.lobbysystem.commands;

import eu.thesimplecloud.api.CloudAPI;
import eu.thesimplecloud.api.player.ICloudPlayer;
import net.minebaum.baumapi.utils.Data;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.awt.image.DataBufferShort;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class COMMAND_Onlinetime implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player p = (Player) commandSender;
        if(p.hasPermission("system.onlinetime")) {
            if(strings.length == 0) {
                p.sendMessage(Data.PREFIX + "§7/onlinetime <Spieler>");
            } else if(strings.length == 1) {
                Player z = Bukkit.getPlayer(strings[0]);
                if(z == null) {
                    p.sendMessage(Data.PREFIX + "§cDieser Spieler ist nicht online§8.");
                } else {
                    ICloudPlayer player = CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(z.getUniqueId());
                    long millis = player.getOnlineTime();
                    Integer hours = Math.toIntExact(TimeUnit.MILLISECONDS.toHours(millis));
                    p.sendMessage(Data.PREFIX + "§7Der Spieler §e" + z.getName() + " §7hat eine Onlinezeit von §e" + hours + " §7Stunden§8.");
                }
            } else {
                p.sendMessage(Data.PREFIX + "§7/onlinetime <Spieler>");
            }
        } else {
            p.sendMessage(Data.NOPERMS);
        }
        return false;
    }
}
