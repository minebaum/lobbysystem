package net.minebaum.lobbysystem.commands;

import net.minebaum.baumapi.utils.Data;
import net.minebaum.lobbysystem.utils.InventoryM;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import java.util.ArrayList;

public class COMMAND_Build implements CommandExecutor , Listener {
    private static ArrayList<Player> build = new ArrayList<>();
    @Override
    public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
        Player p = (Player) s;
        if(p.hasPermission("system.build")) {
            if(build.contains(p)) {
                build.remove(p);
                p.sendMessage(Data.PREFIX + "§cDu hast den BuildModus verlassen§8.");
                p.setGameMode(GameMode.ADVENTURE);
                p.getInventory().clear();
                new InventoryM(p).onsetInv();
            } else {
                build.add(p);
                p.sendMessage(Data.PREFIX + "§aDu hast den BuildMouds betreten§8.");
                p.getInventory().clear();
                p.setGameMode(GameMode.CREATIVE);
            }
        }else {
            p.sendMessage(Data.NOPERMS);
        }
        return false;
    }

    @EventHandler
    public void onblockplace(BlockPlaceEvent e) {
        if(build.contains(e.getPlayer())) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void inv(PlayerInteractEvent e) {
        if(build.contains(e.getPlayer())) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void drop(PlayerDropItemEvent e) {
        if(build.contains(e.getPlayer())) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void pickup(PlayerPickupItemEvent e) {
        if(build.contains(e.getPlayer())) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void inventory(InventoryClickEvent e) {
        if(build.contains(e.getWhoClicked())) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
}
