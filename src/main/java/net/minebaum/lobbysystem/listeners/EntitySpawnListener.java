package net.minebaum.lobbysystem.listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public class EntitySpawnListener implements Listener {

    @EventHandler
    public void onSpawn(EntitySpawnEvent e) {
        if(e.getEntityType().equals(EntityType.PLAYER)|| e.getEntityType().equals(EntityType.ARMOR_STAND)|| e.getEntityType().equals(EntityType.VILLAGER)) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
}
