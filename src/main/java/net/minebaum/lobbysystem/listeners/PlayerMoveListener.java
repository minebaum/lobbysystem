package net.minebaum.lobbysystem.listeners;

import net.minebaum.baumapi.utils.Data;
import net.minebaum.lobbysystem.utils.LocationManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onmove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        if(p.getLocation().getBlock().getType() == Material.IRON_PLATE) {
            if(p.getLocation().subtract(0.0D, 1.0D, 0.0).getBlock().getType() == Material.EMERALD_BLOCK) {
                Vector v = p.getLocation().getDirection().multiply(3.5D).setY(1.0D);
                p.setVelocity(v);
                p.playSound(p.getLocation(), Sound.ENTITY_FIREWORK_BLAST_FAR, 1, 1);
            }
        }
        if(p.getLocation().getBlock().getType() == Material.IRON_PLATE) {
            if(p.getLocation().subtract(0.0D, 1.0D, 0.0).getBlock().getType() == Material.DIAMOND_BLOCK) {
                Vector v = p.getLocation().getDirection().multiply(6D).setY(1.0D);
                p.setVelocity(v);
                p.playSound(p.getLocation(), Sound.ENTITY_FIREWORK_BLAST_FAR, 1, 1);
            }
        }
        if(p.getLocation().getBlock().getType() == Material.IRON_PLATE) {
            if(p.getLocation().subtract(0.0D, 1.0D, 0.0).getBlock().getType() == Material.GOLD_BLOCK) {
                Vector v = p.getLocation().getDirection().multiply(3.5D).setY(2.0D);
                p.setVelocity(v);
                p.playSound(p.getLocation(), Sound.ENTITY_FIREWORK_BLAST_FAR, 1, 1);
            }
        }
        if(p.getLocation().getBlockY() <= 0) {
            Location loc = LocationManager.getLocation("spawn");
            p.teleport(loc);
        }
    }
}
