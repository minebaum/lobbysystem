package net.minebaum.lobbysystem.listeners;

import net.minebaum.lobbysystem.LobbySystem;
import net.minebaum.lobbysystem.mysql.*;
import net.minebaum.lobbysystem.utils.InventoryM;
import net.minebaum.lobbysystem.utils.LocationManager;
import net.minebaum.lobbysystem.utils.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.Scoreboard;

public class JoinListener implements Listener {


    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        Player p = e.getPlayer();
        ScoreboardManager.set(p);
        p.getInventory().clear();
        p.setMaxHealth(2);
        p.setFoodLevel(20);
        p.setLevel(2021);
        Location loc = LocationManager.getLocation("spawn");
        p.teleport(loc);
        new InventoryM(p).onsetInv();
        p.setGameMode(GameMode.ADVENTURE);
        Bukkit.getScheduler().runTaskTimerAsynchronously(LobbySystem.instance ,() -> {
            p.getWorld().setFullTime(1000);
        },0,20*60*5);
        setupMySQL(p);
    }

    private void setupMySQL(Player p) {
        SQLTon.createPlayer(p.getUniqueId().toString());
        SQLFly.createPlayer(p.getUniqueId().toString());
        SQLTeleport.createPlayer(p.getUniqueId().toString());
        SQLSpielerverstecker.createPlayer(p.getUniqueId().toString());
        SQLDaily.createPlayerSpielerReward(p.getUniqueId().toString());
        if(SQLFly.getState(p.getUniqueId().toString()) == 1) {

        }
        
        if(SQLSpielerverstecker.getState(p.getUniqueId().toString()) == 1) {
            for(Player all : Bukkit.getOnlinePlayers()) {
                p.showPlayer(all);
            }
        } else if(SQLSpielerverstecker.getState(p.getUniqueId().toString()) == 2) {
            for(Player all : Bukkit.getOnlinePlayers()) {
                if(all.hasPermission("system.team")) {
                    p.showPlayer(all);
                } else {
                    p.hidePlayer(all);
                }
            }
        }else if(SQLSpielerverstecker.getState(p.getUniqueId().toString()) == 3) {
            for(Player all : Bukkit.getOnlinePlayers()) {
                p.hidePlayer(all);
            }
        }
        for(Player all : Bukkit.getOnlinePlayers()) {
            if(SQLSpielerverstecker.getState(all.getUniqueId().toString()) == 1) {
                    all.showPlayer(p);

            } else if(SQLSpielerverstecker.getState(all.getUniqueId().toString()) == 2) {
                if(p.hasPermission("system.team")) {
                    all.showPlayer(p);
                } else {
                    all.hidePlayer(p);
                }
            } else if(SQLSpielerverstecker.getState(all.getUniqueId().toString()) == 3) {
                    all.hidePlayer(p);
            }
        }
    }
}
