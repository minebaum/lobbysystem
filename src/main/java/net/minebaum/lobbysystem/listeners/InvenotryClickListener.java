package net.minebaum.lobbysystem.listeners;

import eu.thesimplecloud.api.CloudAPI;
import eu.thesimplecloud.api.player.ICloudPlayer;
import eu.thesimplecloud.api.service.ICloudService;
import eu.thesimplecloud.api.service.ServiceState;
import eu.thesimplecloud.api.servicegroup.ICloudServiceGroup;
import eu.thesimplecloud.module.permission.PermissionPool;
import eu.thesimplecloud.module.permission.player.IPermissionPlayer;
import net.minebaum.baumapi.api.GuiAPI;
import net.minebaum.baumapi.utils.Data;
import net.minebaum.baumapi.utils.ItemBuilder;
import net.minebaum.lobbysystem.LobbySystem;
import net.minebaum.lobbysystem.mysql.*;
import net.minebaum.lobbysystem.utils.InventoryM;
import net.minebaum.lobbysystem.utils.shop.InventorysShop;
import net.minebaum.lobbysystem.utils.LocationManager;
import net.minebaum.lobbysystem.utils.ScoreboardManager;
import net.minebaum.lobbysystem.utils.shop.ShopManager;
import net.minebaum.playerapi.MBPlayer;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class InvenotryClickListener implements Listener {


    @EventHandler
    public void onInventory(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        IPermissionPlayer permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPermissionPlayer(p.getUniqueId());
        if(e.getClickedInventory() == null){
            return;
        }
        if(e.getCurrentItem() == null){
            return;
        }
        if(e.getClick().equals(ClickType.LEFT)){
            if(e.getInventory().getTitle().equals(Data.PREFIX +"§cTeleporter")) {
                if(e.getCurrentItem().getType().equals(Material.NETHER_STAR)) {
                    if(SQLTeleport.getState(p.getUniqueId().toString()) == 1) {
                        startTeleportAnimation( "spawn",p);
                        p.sendTitle("§7Teleportiert","§cSPAWN");
                    } else if(SQLTeleport.getState(p.getUniqueId().toString()) == 2) {
                        startTeleport("spawn",p);
                    }
                } else if(e.getCurrentItem().getType().equals(Material.SANDSTONE) && e.getCurrentItem().getItemMeta().getDisplayName().equals("§eBuildFFA §7(Rechtsklick)")) {
                    ICloudServiceGroup serviceGroup = CloudAPI.getInstance().getCloudServiceGroupManager().getServiceGroupByName("BuildFFA");
                    MBPlayer player = new MBPlayer(p);
                    boolean alreadyDid = false;
                    for(ICloudService service : serviceGroup.getAllServices()){
                        if(alreadyDid) return;
                        player.getCloudPlayer().connect(service);
                        alreadyDid = true;
                    }
                } else if(e.getCurrentItem().getType().equals(Material.STICK)) {
                    ICloudServiceGroup serviceGroup = CloudAPI.getInstance().getCloudServiceGroupManager().getServiceGroupByName("MLGRush");
                    MBPlayer player = new MBPlayer(p);
                    boolean alreadyDid = false;
                    for(ICloudService service : serviceGroup.getAllServices()){
                        if(alreadyDid) return;
                        player.getCloudPlayer().connect(service);
                        alreadyDid = true;
                    }
                } else if(e.getCurrentItem().getType().equals(Material.WOOD_AXE)) {
                    ICloudServiceGroup serviceGroup = CloudAPI.getInstance().getCloudServiceGroupManager().getServiceGroupByName("GunGame");
                    MBPlayer player = new MBPlayer(p);
                    boolean alreadyDid = false;
                    for(ICloudService service : serviceGroup.getAllServices()){
                        if(alreadyDid) return;
                        player.getCloudPlayer().connect(service);
                        alreadyDid = true;
                    }
                }
            }
        }
        if(e.getClick().equals(ClickType.RIGHT)) {
            if(e.getInventory().getTitle().startsWith("Shop")){

            }
            if(e.getInventory().getTitle().equals(Data.PREFIX +"§cTeleporter")) {
                if(e.getCurrentItem().getType().equals(Material.NETHER_STAR)) {
                        if(SQLTeleport.getState(p.getUniqueId().toString()) == 1) {
                            startTeleportAnimation( "spawn",p);
                            p.sendTitle("§7Teleportiert","§cSPAWN");
                        } else if(SQLTeleport.getState(p.getUniqueId().toString()) == 2) {
                            startTeleport("spawn",p);
                        }

                } else if(e.getCurrentItem().getType().equals(Material.SANDSTONE) && e.getCurrentItem().getItemMeta().getDisplayName().equals("§eBuildFFA §7(Rechtsklick)")) {
                    if(SQLTeleport.getState(p.getUniqueId().toString()) == 1) {
                        startTeleportAnimation( "buildffaloc",p);
                    } else if(SQLTeleport.getState(p.getUniqueId().toString()) == 2) {
                        startTeleport("buildffaloc",p);
                    }
                } else if(e.getCurrentItem().getType().equals(Material.STICK)) {
                    if(SQLTeleport.getState(p.getUniqueId().toString()) == 1) {
                        startTeleportAnimation( "citybuildloc",p);
                    } else if(SQLTeleport.getState(p.getUniqueId().toString()) == 2) {
                        startTeleport("citybuildloc",p);
                    }
                } else if(e.getCurrentItem().getType().equals(Material.WOOD_AXE)) {
                    if(SQLTeleport.getState(p.getUniqueId().toString()) == 1) {
                        startTeleportAnimation( "gungameloc",p);
                    } else if(SQLTeleport.getState(p.getUniqueId().toString()) == 2) {
                        startTeleport("gungameloc",p);
                    }
                }  else if(e.getCurrentItem().getType().equals(Material.GOLDEN_CARROT)) {
                    if(SQLTeleport.getState(p.getUniqueId().toString()) == 1) {
                        startTeleportAnimation( "2",p);
                        p.sendTitle("§7Teleportiert","§aTÄGLICHE BELOHNUNG");
                    } else if(SQLTeleport.getState(p.getUniqueId().toString()) == 2) {
                        startTeleport("2",p);
                    }
                }
            } else  if(e.getInventory().getTitle().equals(Data.PREFIX +"§eLobbys")) {
                if(e.getCurrentItem().getItemMeta().getLore().contains("§cDort befindest du dich gerade§8.")){
                    p.sendMessage(Data.PREFIX + "§cDu bist schon auf dieser Lobby§8.");
                    p.closeInventory();
                    e.setCancelled(true);
                } else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eLobby-1  §8▪ §7(Rechtsklick)")){
                    startsendtoLobby(p,"Lobby-1");

                } else  if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eLobby-2  §8▪ §7(Rechtsklick)")){
                    startsendtoLobby(p,"Lobby-2");
                } else  if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eLobby-3  §8▪ §7(Rechtsklick)")){
                    startsendtoLobby(p,"Lobby-3");
                } else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eLobby-4  §8▪ §7(Rechtsklick)")){
                    startsendtoLobby(p,"Lobby-4");
                }  else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eLobby-5  §8▪ §7(Rechtsklick)")){
                    startsendtoLobby(p,"Lobby-5");
                }  else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eLobby-6  §8▪ §7(Rechtsklick)")){
                    startsendtoLobby(p,"Lobby-6");
                }  else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eLobby-7  §8▪ §7(Rechtsklick)")){
                    startsendtoLobby(p,"Lobby-7");
                }

            } else if(e.getInventory().getTitle().equals(Data.PREFIX +"§eEinstellungen")) {
                if(e.getCurrentItem().getType().equals(Material.SPECKLED_MELON)) {
                    p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_STEP, 3, 3);
                    new InventoryM(p).getInv().setItem(13, new ItemBuilder(Material.REDSTONE, 1, (short)0).setDisplayname("§cVerstecker §7× §5TEAM").build());
                    for(Player all : Bukkit.getOnlinePlayers()) {
                        if (all.hasPermission("system.team")) {
                            p.showPlayer(all);
                        } else {
                            p.hidePlayer(all);
                        }
                    }
                    SQLSpielerverstecker.setState(p.getUniqueId().toString(), 2);
                    p.closeInventory();
                } else if(e.getCurrentItem().getType().equals(Material.REDSTONE)) {
                    p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_STEP, 3, 3);
                    new InventoryM(p).getInv().setItem(13, new ItemBuilder(Material.CLAY_BALL, 1, (short)0).setDisplayname("§cVerstecker §7× §7NIEMAND").build());
                    for(Player all : Bukkit.getOnlinePlayers()) {
                        p.hidePlayer(all);
                    }
                    SQLSpielerverstecker.setState(p.getUniqueId().toString(), 3);
                    p.closeInventory();
                }  else if(e.getCurrentItem().getType().equals(Material.CLAY_BALL)) {
                    p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_STEP, 3, 3);
                    new InventoryM(p).getInv().setItem(13, new ItemBuilder(Material.SPECKLED_MELON, 1, (short)0).setDisplayname("§cVerstecker §7× §aALLE").build());
                    SQLSpielerverstecker.setState(p.getUniqueId().toString(), 1);
                    for(Player all : Bukkit.getOnlinePlayers()) {
                        p.showPlayer(all);
                    }
                    SQLSpielerverstecker.setState(p.getUniqueId().toString(), 1);
                    p.closeInventory();
                } else if(e.getCurrentItem().getType().equals(Material.REDSTONE_TORCH_ON)) {
                    new InventoryM(p).openlobbysetting();
                }
            } else if(e.getInventory().getTitle().equals(Data.PREFIX + "§cLobby-Einstellungen")) {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equals("§cHotbar §8» §aAN")) {
                    new InventoryM(p).getLobbysetting().setItem(2, new ItemBuilder(Material.WOOD_BUTTON, 1, (short) 0).setDisplayname("§cHotbar §8» §cAUS").build());
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 5);
                    SQLTon.setState(p.getUniqueId().toString(), 2);
                    p.closeInventory();
                } else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("§cHotbar §8» §cAUS")) {
                    new InventoryM(p).getLobbysetting().setItem(2, new ItemBuilder(Material.WOOD_BUTTON, 1, (short) 0).setDisplayname("§cHotbar §8» §aAN").build());
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 5);
                    SQLTon.setState(p.getUniqueId().toString(),1);
                    p.closeInventory();
                } else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("§5Animation §8» §aAN")) {
                    new InventoryM(p).getLobbysetting().setItem(6, new ItemBuilder(Material.ENDER_PEARL, 1, (short) 0).setDisplayname("§5Teleport §8» §cAUS").build());
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 5);
                    SQLTeleport.setState(p.getUniqueId().toString(), 2);
                    p.closeInventory();
                } else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("§5Animation §8» §cAUS")) {
                    new InventoryM(p).getLobbysetting().setItem(6, new ItemBuilder(Material.ENDER_PEARL, 1, (short) 0).setDisplayname("§cHotbar §8» §aAN").build());
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 5);
                    SQLTeleport.setState(p.getUniqueId().toString(),1);
                    p.closeInventory();
                }
            } else if(e.getInventory().getTitle().equals(Data.PREFIX + "§aTäglichebelohnung")) {
                if(e.getCurrentItem().getType().equals(Material.HARD_CLAY)) {
                    SQLDaily.setSpielerReward(p,p.getUniqueId().toString());
                    ScoreboardManager.set(p);
                } else     if(e.getCurrentItem().getType().equals(Material.GOLD_INGOT)) {
                    SQLDaily.setPremiumReward(p,p.getUniqueId().toString());
                    ScoreboardManager.set(p);
                } else     if(e.getCurrentItem().getType().equals(Material.DIAMOND)) {
                    SQLDaily.setBaumReward(p,p.getUniqueId().toString());
                    ScoreboardManager.set(p);
                }
            } else {
                e.setCancelled(true);
            }
        }
    }
    private  void startTeleportAnimation(String locs ,Player p) {
            p.playSound(p.getLocation(), Sound.ENTITY_FIREWORK_BLAST_FAR, 5, 3);
            p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20*2, 2555));
            p.setVelocity(new Vector(0,2,0));
            p.closeInventory();
            Bukkit.getScheduler().runTaskLater(LobbySystem.instance, ()-> {
                Location loc = LocationManager.getLocation(locs);
                p.teleport(loc);
                p.playSound(p.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1, 3);
            },20*1);
    }
        private void startTeleport(String locs, Player p) {
            p.sendTitle(null,null);
            p.getActivePotionEffects().removeAll(p.getActivePotionEffects());
            Location loc = LocationManager.getLocation(locs);
            p.teleport(loc);p.playSound(p.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1, 3);

    }
    private  void startsendtoLobby(Player p, String lobby) {
        ICloudService service = CloudAPI.getInstance().getCloudServiceManager().getCloudServiceByName(lobby);
        ICloudPlayer player = CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(p.getUniqueId());
        assert service != null;
        if(service.getState().equals(ServiceState.VISIBLE)) {
            assert player != null;
            p.sendMessage(Data.PREFIX + "§aDu wurdest mit §e"+lobby +"§a verbunden§8.");
            player.connect(service);
            p.closeInventory();
        } else {
            p.sendMessage(Data.PREFIX + "§cDieser Server startet noch§8.");
            p.closeInventory();
        }

    }
    private void startopenschopinv() {
        Inventory inv = new GuiAPI().fillerGUI(9,new ItemBuilder(Material.STAINED_GLASS_PANE, 1 , (short) 15).build() , Data.PREFIX +"§7Rang");
    }
    @EventHandler
    public void onInventoryShop(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if(e.getClickedInventory() == null){
            return;
        }
        if(e.getCurrentItem() == null){
            return;
        }
        if(e.getClick().equals(ClickType.LEFT)|| e.getClick().equals(ClickType.RIGHT)) {
            if(e.getInventory().equals(e.getInventory())){
                if(e.getCurrentItem().getType() == Material.FEATHER){
                    if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7FLY §8× §cOFF")){
                        SQLFly.setState(((Player) e.getWhoClicked()).getPlayer().getUniqueId().toString(), 2);
                        new InventoryM(((Player) e.getWhoClicked()).getPlayer()).onsetInv();
                    }else
                    if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7FLY §8× §aON")){
                        SQLFly.setState(((Player) e.getWhoClicked()).getPlayer().getUniqueId().toString(), 1);
                        new InventoryM(((Player) e.getWhoClicked()).getPlayer()).onsetInv();
                    }
                }
            }
            if(e.getInventory().getTitle().equals(Data.PREFIX +"§eEinstellungen")) {
                if(e.getCurrentItem().getType() == Material.GOLD_INGOT ||e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§e§lShop")) {
                    ShopManager.openShopSite(ShopManager.RANK_PREMIUM, p);
                }
            }else{
                ShopManager.executeClickEvent(e.getCurrentItem(), e.getInventory(), p);
            }
        }

    }
}
