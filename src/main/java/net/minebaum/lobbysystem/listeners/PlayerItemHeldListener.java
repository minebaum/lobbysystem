package net.minebaum.lobbysystem.listeners;

import net.minebaum.lobbysystem.mysql.SQLTon;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemHeldEvent;

public class PlayerItemHeldListener implements Listener {

    @EventHandler
    public void onItemHeld(PlayerItemHeldEvent e) {
        Player p = e.getPlayer();
        if(SQLTon.getState(p.getUniqueId().toString()) == 1) {
            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_COMPARATOR_CLICK, 1, 3);
        } else if(SQLTon.getState(p.getUniqueId().toString()) == 2) {
        }
    }
}
