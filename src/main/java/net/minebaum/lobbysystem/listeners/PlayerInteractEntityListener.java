package net.minebaum.lobbysystem.listeners;

import eu.thesimplecloud.api.CloudAPI;
import eu.thesimplecloud.api.player.ICloudPlayer;
import eu.thesimplecloud.api.service.ICloudService;
import eu.thesimplecloud.api.service.ServiceState;
import net.minebaum.baumapi.api.GuiAPI;
import net.minebaum.baumapi.utils.Data;
import net.minebaum.baumapi.utils.ItemBuilder;
import net.minebaum.lobbysystem.mysql.SQLDaily;
import net.minebaum.lobbysystem.utils.LocationManager;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.util.Vector;

public class PlayerInteractEntityListener implements Listener {

    @EventHandler
    public void oninteract(PlayerInteractAtEntityEvent e) {
        Player p = e.getPlayer();
        e.setCancelled(true);
        if (e.getRightClicked().getType().equals(EntityType.ARMOR_STAND)) {
            if (e.getRightClicked().getLocation().getX() == LocationManager.getLocation("buildffa").getX()) {
                ICloudService servicebuildffa = CloudAPI.getInstance().getCloudServiceManager().getCloudServiceByName("BuildFFA-1");
                ICloudPlayer player = CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(p.getUniqueId());
                if (servicebuildffa.getState().equals(ServiceState.VISIBLE)) {
                    player.connect(servicebuildffa);
                    p.sendMessage(Data.PREFIX + "§aDu wurdest zu BuildFFA gesendet§8.");
                } else if (servicebuildffa.getState().equals(ServiceState.CLOSED)) {
                    p.sendMessage(Data.PREFIX + "§cDer Server ist offline§8.");
                    p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE, 1, 3);
                    Vector v = p.getLocation().getDirection().multiply(-1.5D).setY(1.0D);
                    p.setVelocity(v);
                } else if (servicebuildffa.getState().equals(ServiceState.STARTING)) {
                    p.sendMessage(Data.PREFIX + "§cDer Server ist am starten§8.");
                    p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE, 1, 3);
                    Vector v = p.getLocation().getDirection().multiply(-1.5D).setY(1.0D);
                    p.setVelocity(v);
                }
            } else if (e.getRightClicked().getLocation().getX() == LocationManager.getLocation("citybuild").getX()) {
                ICloudService servicebuildffa = CloudAPI.getInstance().getCloudServiceManager().getCloudServiceByName("CityBuild-1");
                ICloudPlayer player = CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(p.getUniqueId());
                if (servicebuildffa.getState().equals(ServiceState.VISIBLE)) {
                    if (p.hasPermission("system.join")) {
                        player.connect(servicebuildffa);
                        p.sendMessage(Data.PREFIX + "§aDu wurdest zu BuildFFA gesendet§8.");
                    } else {
                        p.sendMessage(Data.PREFIX + "§7Wartungen");
                    }
                } else if (servicebuildffa.getState().equals(ServiceState.CLOSED)) {
                    p.sendMessage(Data.PREFIX + "§cDer Server ist offline§8.");
                    p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE, 1, 3);
                    Vector v = p.getLocation().getDirection().multiply(-1.5D).setY(1.0D);
                    p.setVelocity(v);
                } else if (servicebuildffa.getState().equals(ServiceState.STARTING)) {
                    p.sendMessage(Data.PREFIX + "§cDer Server ist am starten§8.");
                    p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE, 1, 3);
                    Vector v = p.getLocation().getDirection().multiply(-1.5D).setY(1.0D);
                    p.setVelocity(v);
                }
            } else if (e.getRightClicked().getLocation().getX() == LocationManager.getLocation("gungame").getX()) {
                ICloudService servicebuildffa = CloudAPI.getInstance().getCloudServiceManager().getCloudServiceByName("GunGame-1");
                ICloudPlayer player = CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(p.getUniqueId());
                if (servicebuildffa.getState().equals(ServiceState.VISIBLE)) {
                    player.connect(servicebuildffa);
                    p.sendMessage(Data.PREFIX + "§aDu wurdest zu GunGame gesendet§8.");
                } else if (servicebuildffa.getState().equals(ServiceState.CLOSED)) {
                    p.sendMessage(Data.PREFIX + "§cDer Server ist offline§8.");
                    p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE, 1, 3);
                    Vector v = p.getLocation().getDirection().multiply(-1.5D).setY(1.0D);
                    p.setVelocity(v);
                } else if (servicebuildffa.getState().equals(ServiceState.STARTING)) {
                    p.sendMessage(Data.PREFIX + "§cDer Server ist am starten§8.");
                    p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE, 1, 3);
                    Vector v = p.getLocation().getDirection().multiply(-1.5D).setY(1.0D);
                    p.setVelocity(v);
                }
                } else if (e.getRightClicked().getLocation().getX() == LocationManager.getLocation("mlgrush").getX()) {
                    ICloudService servicebuildffa = CloudAPI.getInstance().getCloudServiceManager().getCloudServiceByName("MLGRush-1");
                    ICloudPlayer cp = CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(p.getUniqueId());
                    if (servicebuildffa.getState().equals(ServiceState.VISIBLE)) {
                        cp.connect(servicebuildffa);
                        p.sendMessage(Data.PREFIX + "§aDu wurdest zu MLGRush gesendet§8.");
                    } else if (servicebuildffa.getState().equals(ServiceState.CLOSED)) {
                        p.sendMessage(Data.PREFIX + "§cDer Server ist offline§8.");
                        p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE, 1, 3);
                        Vector v = p.getLocation().getDirection().multiply(-1.5D).setY(1.0D);
                        p.setVelocity(v);
                    } else if (servicebuildffa.getState().equals(ServiceState.STARTING)) {
                        p.sendMessage(Data.PREFIX + "§cDer Server ist am starten§8.");
                        p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE, 1, 3);
                        Vector v = p.getLocation().getDirection().multiply(-1.5D).setY(1.0D);
                        p.setVelocity(v);
                    }

            }
        }else if (e.getRightClicked().getType().equals(EntityType.VILLAGER)) {
                openDailyRewardinv(e.getPlayer());
            }
        }

    @EventHandler
    public void onplayer(PlayerInteractEntityEvent e){
        e.setCancelled(true);
    }

    private static void openDailyRewardinv(Player p) {
        Inventory inv = new GuiAPI().fillerGUI(9, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,(short) 15).setDisplayname(" ").build(),Data.PREFIX + "§aTäglichebelohnung");
        inv.setItem(2, new ItemBuilder(Material.DIAMOND, 1,(short)0).setDisplayname("§aBaum §8× §eBelohnug").build());
        inv.setItem(4, new ItemBuilder(Material.HARD_CLAY, 1,(short)0).setDisplayname("§7Spieler §8× §eBelohnug").build());
        inv.setItem(6, new ItemBuilder(Material.GOLD_INGOT, 1,(short)0).setDisplayname("§6Premium §8× §eBelohnug").build());
        p.openInventory(inv);
    }
}
