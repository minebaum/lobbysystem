package net.minebaum.lobbysystem.listeners;

import eu.thesimplecloud.api.CloudAPI;
import eu.thesimplecloud.api.player.ICloudPlayer;
import eu.thesimplecloud.api.service.ICloudService;
import eu.thesimplecloud.api.servicegroup.ICloudServiceGroup;
import eu.thesimplecloud.plugin.startup.CloudPlugin;
import net.minebaum.baumapi.api.GuiAPI;
import net.minebaum.baumapi.utils.Data;
import net.minebaum.baumapi.utils.ItemBuilder;
import net.minebaum.lobbysystem.LobbySystem;
import net.minebaum.lobbysystem.mysql.SQLSpielerverstecker;
import net.minebaum.lobbysystem.mysql.SQLTeleport;
import net.minebaum.lobbysystem.utils.InventoryM;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;

public class PlayerInteractListener implements Listener {

    public static BukkitTask task1;
    public static BukkitTask task2;
    public static BukkitTask task3;
    public static BukkitTask task4;
    public static BukkitTask task5;

    @EventHandler
    public void interactevent(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if(e.getAction().equals(Action.RIGHT_CLICK_AIR)|| e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if(e.getMaterial().equals(Material.COMPASS)) {
                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP , 1,5);
                    openteleportinv(p);
            } else if(e.getMaterial().equals(Material.RECORD_7)){
                    openlobbyinv(p);
            } else if(e.getMaterial().equals(Material.REDSTONE_COMPARATOR)) {
                    new InventoryM(p).opensettingsinv();
            } else {
                e.setCancelled(true);
            }
        } else {
            e.setCancelled(true);
        }
    }

    private void openteleportinv(Player p) {
        if(SQLTeleport.getState(p.getUniqueId().toString()) == 1) {
            Inventory inv = new GuiAPI().fillerGUI(54, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 15).setDisplayname(" ").build(), "§cTeleporter");
            ItemStack redglass = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setDisplayname(" ").build();
            ItemStack whiteglass = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 0).setDisplayname(" ").build();
            ItemStack greyglass = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 7).setDisplayname(" ").build();
            inv.setItem(0, whiteglass);
            inv.setItem(1, whiteglass);
            inv.setItem(9, whiteglass);
            inv.setItem(7, whiteglass);
            inv.setItem(8, whiteglass);
            inv.setItem(17, whiteglass);
            inv.setItem(36, whiteglass);
            inv.setItem(44, whiteglass);
            inv.setItem(46, whiteglass);
            inv.setItem(52, whiteglass);
            inv.setItem(3, greyglass);
            inv.setItem(5, greyglass);
            inv.setItem(11, greyglass);
            inv.setItem(15, greyglass);
            inv.setItem(19, greyglass);
            inv.setItem(25, greyglass);
            inv.setItem(27, greyglass);
            inv.setItem(35, greyglass);
            inv.setItem(37, greyglass);
            inv.setItem(43, greyglass);
            inv.setItem(46, greyglass);
            inv.setItem(50, greyglass);
            inv.setItem(22, redglass);
            inv.setItem(30, redglass);
            inv.setItem(31, redglass);
            inv.setItem(32, redglass);
            task1 = Bukkit.getScheduler().runTaskLater(LobbySystem.instance, () ->{
                ICloudServiceGroup serviceGroup = CloudAPI.getInstance().getCloudServiceGroupManager().getServiceGroupByName("Lobby");
                final int onlinePlayers = serviceGroup.getOnlinePlayerCount();
                List<String> lore = new ArrayList<>();
                lore.add("§7Spieler §8» §c" + onlinePlayers);
                lore.add("§7QuickJoin (Linksklick)");
                inv.setItem(13, new ItemBuilder(Material.NETHER_STAR , onlinePlayers, (short) 0).setLore(lore).setDisplayname("§cSPAWN §7(Rechtsklick)").build());
                // inv.setItem(6, new ItemBuilder(Material.GOLDEN_CARROT , 1, (short) 0).setDisplayname("§eTAGLICHE BELOHNUNG §7(Rechtsklick)").build());
                p.playSound(p.getLocation(), Sound.ENTITY_BAT_TAKEOFF, 1 ,5);
            },10*2);
            task2 = Bukkit.getScheduler().runTaskLater(LobbySystem.instance , () -> {
                //inv.setItem(20, new ItemBuilder(Material.SANDSTONE , 1, (short) 0).setDisplayname("§eBuildFFA §7(Rechtsklick)").build());
                //inv.setItem(18, new ItemBuilder(Material.WOOD_AXE , 1, (short) 0).setDisplayname("§eGunGame §7(Rechtsklick)").build());
                ICloudServiceGroup serviceGroup = CloudAPI.getInstance().getCloudServiceGroupManager().getServiceGroupByName("MLGRush");
                final int onlinePlayers = serviceGroup.getOnlinePlayerCount();
                List<String> lore = new ArrayList<>();
                lore.add("§7Spieler §8» §c" + onlinePlayers);
                lore.add("§7QuickJoin (Linksklick)");
                inv.setItem(29, new ItemBuilder(Material.STICK , onlinePlayers, (short) 0).setLore(lore).setDisplayname("§eMLGRush §7(Rechtsklick)").build());
                p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BELL, 1 ,5);
            }, 35);
            task3 = Bukkit.getScheduler().runTaskLater(LobbySystem.instance , () -> {
                ICloudServiceGroup serviceGroup = CloudAPI.getInstance().getCloudServiceGroupManager().getServiceGroupByName("BuildFFA");
                final int onlinePlayers = serviceGroup.getOnlinePlayerCount();
                List<String> lore = new ArrayList<>();
                lore.add("§7Spieler §8» §c" + onlinePlayers);
                lore.add("§7QuickJoin (Linksklick)");
                inv.setItem(40, new ItemBuilder(Material.SANDSTONE , onlinePlayers, (short) 0).setLore(lore).setDisplayname("§eBuildFFA §7(Rechtsklick)").build());
                //inv.setItem(33, new ItemBuilder(Material.WOOD_AXE , 1, (short) 0).setDisplayname("§eGunGame §7(Rechtsklick)").build());
                //inv.setItem(8, new ItemBuilder(Material.WORKBENCH , 1, (short) 0).setDisplayname("§eCityBuild §7(Rechtsklick)").build());
                p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BELL, 1 ,5);
            }, 40);
            task4 = Bukkit.getScheduler().runTaskLater(LobbySystem.instance , () -> {
                ICloudServiceGroup serviceGroup = CloudAPI.getInstance().getCloudServiceGroupManager().getServiceGroupByName("GunGame");
                final int onlinePlayers = serviceGroup.getOnlinePlayerCount();
                List<String> lore = new ArrayList<>();
                lore.add("§7Spieler §8» §c" + onlinePlayers);
                lore.add("§7QuickJoin (Linksklick)");
                //inv.setItem(20, new ItemBuilder(Material.SANDSTONE , 1, (short) 0).setDisplayname("§eBuildFFA §7(Rechtsklick)").build());
                inv.setItem(33, new ItemBuilder(Material.WOOD_AXE , onlinePlayers, (short) 0).setLore(lore).setDisplayname("§eGunGame §7(Rechtsklick)").build());
                //inv.setItem(8, new ItemBuilder(Material.WORKBENCH , 1, (short) 0).setDisplayname("§eCityBuild §7(Rechtsklick)").build());
                p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BELL, 1 ,5);
            }, 45);
            task5 = Bukkit.getScheduler().runTaskLater(LobbySystem.instance , () -> {
                inv.setItem(45, new ItemBuilder(Material.GOLDEN_CARROT , 1, (short) 0).setDisplayname("§eTAGLICHE BELOHNUNG §7(Rechtsklick)").build());
                p.playSound(p.getLocation(), Sound.ENTITY_BAT_TAKEOFF, 1 ,5);
            }, 65);
            p.openInventory(inv);
        } else if(SQLTeleport.getState(p.getUniqueId().toString()) == 2) {
            Inventory inv = new GuiAPI().fillerGUI(54, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 15).setDisplayname(" ").build(), "§cTeleporter");
            ItemStack redglass = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setDisplayname(" ").build();
            ItemStack whiteglass = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 0).setDisplayname(" ").build();
            ItemStack greyglass = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 7).setDisplayname(" ").build();
            inv.setItem(0, whiteglass);
            inv.setItem(1, whiteglass);
            inv.setItem(9, whiteglass);
            inv.setItem(7, whiteglass);
            inv.setItem(8, whiteglass);
            inv.setItem(17, whiteglass);
            inv.setItem(36, whiteglass);
            inv.setItem(44, whiteglass);
            inv.setItem(46, whiteglass);
            inv.setItem(52, whiteglass);
            inv.setItem(3, greyglass);
            inv.setItem(5, greyglass);
            inv.setItem(11, greyglass);
            inv.setItem(15, greyglass);
            inv.setItem(19, greyglass);
            inv.setItem(25, greyglass);
            inv.setItem(27, greyglass);
            inv.setItem(35, greyglass);
            inv.setItem(37, greyglass);
            inv.setItem(43, greyglass);
            inv.setItem(46, greyglass);
            inv.setItem(50, greyglass);
            inv.setItem(22, redglass);
            inv.setItem(30, redglass);
            inv.setItem(31, redglass);
            inv.setItem(32, redglass);
            ICloudServiceGroup serviceGroup = CloudAPI.getInstance().getCloudServiceGroupManager().getServiceGroupByName("Lobby");
            int onlinePlayers = serviceGroup.getOnlinePlayerCount();
            List<String> lore1 = new ArrayList<>();
            lore1.add("§7Spieler §8» §c" + onlinePlayers);
            inv.setItem(13, new ItemBuilder(Material.NETHER_STAR , onlinePlayers, (short) 0).setLore(lore1).setDisplayname("§cSPAWN §7(Rechtsklick)").build());

            ICloudServiceGroup serviceGroup1 = CloudAPI.getInstance().getCloudServiceGroupManager().getServiceGroupByName("MLGRush");
            final int onlinePlayers1 = serviceGroup1.getOnlinePlayerCount();
            List<String> lore2 = new ArrayList<>();
            lore2.add("§7Spieler §8» §c" + onlinePlayers1);
            lore2.add("§7QuickJoin (Linksklick)");
            inv.setItem(29, new ItemBuilder(Material.STICK , onlinePlayers1, (short) 0).setLore(lore2).setDisplayname("§eMLGRush §7(Rechtsklick)").build());

            ICloudServiceGroup serviceGroup2 = CloudAPI.getInstance().getCloudServiceGroupManager().getServiceGroupByName("BuildFFA");
            final int onlinePlayers2 = serviceGroup2.getOnlinePlayerCount();
            List<String> lore3 = new ArrayList<>();
            lore3.add("§7Spieler §8» §c" + onlinePlayers2);
            lore3.add("§7QuickJoin (Linksklick)");
            inv.setItem(40, new ItemBuilder(Material.SANDSTONE , onlinePlayers2, (short) 0).setLore(lore3).setDisplayname("§eBuildFFA §7(Rechtsklick)").build());

            ICloudServiceGroup serviceGroup3 = CloudAPI.getInstance().getCloudServiceGroupManager().getServiceGroupByName("GunGame");
            final int onlinePlayers3 = serviceGroup3.getOnlinePlayerCount();
            List<String> lore4 = new ArrayList<>();
            lore4.add("§7Spieler §8» §c" + onlinePlayers3);
            lore4.add("§7QuickJoin (Linksklick)");
            //inv.setItem(20, new ItemBuilder(Material.SANDSTONE , 1, (short) 0).setDisplayname("§eBuildFFA §7(Rechtsklick)").build());
            inv.setItem(33, new ItemBuilder(Material.WOOD_AXE , onlinePlayers3, (short) 0).setLore(lore4).setDisplayname("§eGunGame §7(Rechtsklick)").build());

            p.playSound(p.getLocation(), Sound.ENTITY_BAT_TAKEOFF, 1 ,5);
            p.openInventory(inv);
        }

    }
    private void openlobbyinv(Player p) {
        ICloudPlayer player = CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(p.getUniqueId());
        ICloudServiceGroup serviceGroup = CloudAPI.getInstance().getCloudServiceGroupManager().getServiceGroupByName("Lobby");
        List<ICloudService> allservices = serviceGroup.getAllServices();
        int onlinelobbys = allservices.size();
        Inventory inv = new GuiAPI().fillerGUI(9*3, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,(short) 15).setDisplayname(" ").build(),Data.PREFIX + "§eLobbys");
        inv.setItem(10,new ItemBuilder(Material.STAINED_GLASS_PANE, 1,(short) 14).setDisplayname(" ").build());
        inv.setItem(11,new ItemBuilder(Material.STAINED_GLASS_PANE, 1,(short) 14).setDisplayname(" ").build());
        inv.setItem(12,new ItemBuilder(Material.STAINED_GLASS_PANE, 1,(short) 14).setDisplayname(" ").build());
        inv.setItem(13,new ItemBuilder(Material.STAINED_GLASS_PANE, 1,(short) 14).setDisplayname(" ").build());
        inv.setItem(14,new ItemBuilder(Material.STAINED_GLASS_PANE, 1,(short) 14).setDisplayname(" ").build());
        inv.setItem(15,new ItemBuilder(Material.STAINED_GLASS_PANE, 1,(short) 14).setDisplayname(" ").build());
        inv.setItem(16,new ItemBuilder(Material.STAINED_GLASS_PANE, 1,(short) 14).setDisplayname(" ").build());
        p.playSound(p.getLocation(), Sound.ENTITY_BAT_TAKEOFF, 1 ,5);
        for(int i = 1; i <= onlinelobbys; i++) {
            ICloudService iCloudService = CloudAPI.getInstance().getCloudServiceManager().getCloudServiceByName("Lobby-" + i);
            int onlinePlayer = iCloudService.getOnlineCount();
            ArrayList<String> lore = new ArrayList<String>();
            lore.add("§eOnline §8● §a" + onlinePlayer);
            lore.add("");
            if(player.getConnectedServer().getName().equalsIgnoreCase("Lobby-"+i)) {
                lore.add("§cDort befindest du dich gerade§8.");
                inv.setItem(i+9,new ItemBuilder(Material.CHEST, 1 ,(short)0).setLore(lore).setDisplayname("§eLobby-"+i + "  §8▪ §7(Rechtsklick)").build());
            } else {
                inv.setItem(i+9,new ItemBuilder(Material.CHEST, 1 ,(short)0).setLore(lore).setDisplayname("§eLobby-"+i + "  §8▪ §7(Rechtsklick)").build());
            }
        }
        p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1, 3);
        p.openInventory(inv);
    }

}
