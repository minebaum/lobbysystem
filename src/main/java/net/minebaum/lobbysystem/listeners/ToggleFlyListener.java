package net.minebaum.lobbysystem.listeners;

import eu.thesimplecloud.module.permission.PermissionPool;
import eu.thesimplecloud.module.permission.player.IPermissionPlayer;
import net.minebaum.lobbysystem.mysql.SQLFly;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;


public class ToggleFlyListener implements Listener {
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player p = event.getPlayer();
        IPermissionPlayer permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPermissionPlayer(p.getUniqueId());
        if (permissionPlayer.getPermissionGroupInfoList().size() > 1 && p.getGameMode() != GameMode.CREATIVE) {
            if (p.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType() != Material.AIR)
                p.setAllowFlight(true);
        }
    }
    @EventHandler
    public void onPlayerToggleFly(PlayerToggleFlightEvent event) {
        Player player = event.getPlayer();
        if (player.getGameMode() == GameMode.CREATIVE)
            return;
        if(SQLFly.playerExists(event.getPlayer().getUniqueId().toString()) && SQLFly.getState(player.getUniqueId().toString()) == 2){
            player.setFlying(true);
            return;
        }else {
            event.setCancelled(true);
            player.setFlying(false);
            player.setAllowFlight(false);

            player.setVelocity(player.getLocation().getDirection().multiply(1.0D * 2).setY(1.0D * 2));
            player.getWorld().playSound(player.getLocation(), Sound.ENTITY_BAT_LOOP, 1.0F, 5.0F);
            player.setFallDistance(0.0F);
        }
    }
}
