package net.minebaum.lobbysystem.mysql;


import eu.thesimplecloud.module.permission.PermissionPool;
import eu.thesimplecloud.module.permission.player.IPermissionPlayer;
import net.minebaum.baumapi.BaumAPI;
import net.minebaum.baumapi.utils.Data;
import net.minebaum.lobbysystem.LobbySystem;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class SQLDaily {
//Spieler
    public static boolean playerExistsSpielerReward(String uuid) {
        try{
            ResultSet rs = LobbySystem.mySQL.query("SELECT * FROM RewardTime WHERE UUID= '"+uuid+"'");
            if(rs.next()) {
                return rs.getString("UUID") != null;
            }
            return false;
        }catch (SQLException exception ){
            exception.printStackTrace();
        }
        return false;
    }
    public static void createPlayerSpielerReward(String uuid) {
        Long current = System.currentTimeMillis() - (24*60*1000*60);
        if(!(playerExistsSpielerReward(uuid))) {
            LobbySystem.mySQL.update("INSERT INTO RewardTime(UUID,RewardTimesSpieler) VALUES ('"+uuid+"','"+current+"');");
        }
    }
    public static Long getSqlSpielerReward(String uuid) {
        long i = 0;
        if(playerExistsSpielerReward(uuid)) {
            try{
                ResultSet rs = LobbySystem.mySQL.query("Select * FROM RewardTime WHERE UUID= '" + uuid + "'");
                if((!rs.next())|| (Long.getLong("RewardTimesSpieler") == null));
                i = rs.getLong("RewardTimesSpieler");
            }catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            createPlayerSpielerReward(uuid);
            getSqlSpielerReward(uuid);
        }
        return i;
    }
    public static void setSqlSpielerReward(String uuid) {
        long current =  (System.currentTimeMillis() + (24*60*1000*60));
         if(playerExistsSpielerReward(uuid)) {
            LobbySystem.mySQL.update("UPDATE RewardTime SET RewardTimesSpieler= '" +current+ "' WHERE UUID= '" + uuid + "'");
        }else {
            createPlayerSpielerReward(uuid);
            setSqlSpielerReward(uuid);
        }
    }
    public static void setSpielerReward(Player p ,String uuid) {
        if(playerExistsSpielerReward(uuid)) {
            if(getSqlSpielerReward(uuid) < System.currentTimeMillis()) {
                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 3);
                p.sendMessage(Data.PREFIX + "§7 Dir wurden §e25 §aBäume §7dazugegeben§8.");
                BaumAPI.getCoinsAPI().addCoins(p,25);
                setSqlSpielerReward(uuid);
                p.closeInventory();
            } else {
                p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE, 1, 3);
                Date date = new Date(getSqlSpielerReward(uuid));
                String mm_dd_yyyy = new SimpleDateFormat("dd.MM.yyyy").format(date);
                String hour_min = new SimpleDateFormat("HH:mm").format(date);
                p.sendMessage(Data.PREFIX + "§cDu musst noch bis zu dem §7"+mm_dd_yyyy + " §cum §7"+ hour_min + " §cwarten§8.");
                p.closeInventory();
            }
        }else {
            createPlayerSpielerReward(uuid);
            setSpielerReward(p,uuid);
        }
    }
//Spieler
//Premium
    public static boolean playerExistsPremiumReward(String uuid) {
        try{
            ResultSet rs = LobbySystem.mySQL.query("SELECT * FROM RewardTime WHERE UUID= '"+uuid+"'");
            if(rs.next()) {
                return rs.getString("UUID") != null;
            }
            return false;
        }catch (SQLException exception ){
            exception.printStackTrace();
        }
        return false;
    }
    public static void createPlayerPremiumReward(String uuid) {
        Long current = System.currentTimeMillis() - (24*60*1000*60);
        if(!(playerExistsPremiumReward(uuid))) {
            LobbySystem.mySQL.update("INSERT INTO RewardTime(UUID,RewardTimesPremium) VALUES ('"+uuid+"','"+current+"');");
        }
    }
    public static Long getSqlPremiumReward(String uuid) {
        long i = 0;
        if(playerExistsPremiumReward(uuid)) {
            try{
                ResultSet rs = LobbySystem.mySQL.query("Select * FROM RewardTime WHERE UUID= '" + uuid + "'");
                if((!rs.next())|| (Long.getLong("RewardTimesPremium") == null));
                i = rs.getLong("RewardTimesPremium");
            }catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            createPlayerPremiumReward(uuid);
            getSqlPremiumReward(uuid);
        }
        return i;
    }
    public static void setSqlPremiumReward(String uuid) {
        long current =  (System.currentTimeMillis() + (24*60*1000*60));
        if(playerExistsPremiumReward(uuid)) {
            LobbySystem.mySQL.update("UPDATE RewardTime SET RewardTimesPremium= '" +current+ "' WHERE UUID= '" + uuid + "'");
        }else {
            createPlayerPremiumReward(uuid);
            setSqlPremiumReward(uuid);
        }
    }
    public static void setPremiumReward(Player p ,String uuid) {
        IPermissionPlayer permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPermissionPlayer(p.getUniqueId());
        if(playerExistsSpielerReward(uuid)) {
                if(permissionPlayer.getPermissionGroupInfoList().size() == 1) {
                    p.closeInventory();
                    p.sendMessage(Data.PREFIX + "§cDu kannst als §7Spieler §ckeine §6Premium §cBelohnungen abholen§8.");
                } else {
                    if(getSqlPremiumReward(uuid) < System.currentTimeMillis()) {
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 3);
                    p.sendMessage(Data.PREFIX + "§7 Dir wurden §e100 §aBäume §7dazugegeben§8.");
                    BaumAPI.getCoinsAPI().addCoins(p,100);
                    setSqlPremiumReward(uuid);
                    p.closeInventory();
                } else {
                    p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE, 1, 3);
                    Date date = new Date(getSqlPremiumReward(uuid));
                    String mm_dd_yyyy = new SimpleDateFormat("dd.MM.yyyy").format(date);
                    String hour_min = new SimpleDateFormat("HH:mm").format(date);
                    p.sendMessage(Data.PREFIX + "§cDu musst noch bis zu dem §7"+mm_dd_yyyy + " §cum §7"+ hour_min + " §cwarten§8.");
                    p.closeInventory();
                }
                }

        }else {
            createPlayerPremiumReward(uuid);
            setPremiumReward(p,uuid);
        }
    }
//Premium
//Baum
public static boolean playerExistsBaumReward(String uuid) {
    try{
        ResultSet rs = LobbySystem.mySQL.query("SELECT * FROM RewardTime WHERE UUID= '"+uuid+"'");
        if(rs.next()) {
            return rs.getString("UUID") != null;
        }
        return false;
    }catch (SQLException exception ){
        exception.printStackTrace();
    }
    return false;
}
    public static void createPlayerBaumReward(String uuid) {
        Long current = System.currentTimeMillis() - (24*60*1000*60);
        if(!(playerExistsBaumReward(uuid))) {
            LobbySystem.mySQL.update("INSERT INTO RewardTime(UUID,RewardTimesBaum) VALUES ('"+uuid+"','"+current+"');");
        }
    }
    public static Long getSqlBaumReward(String uuid) {
        long i = 0;
        if(playerExistsBaumReward(uuid)) {
            try{
                ResultSet rs = LobbySystem.mySQL.query("Select * FROM RewardTime WHERE UUID= '" + uuid + "'");
                if((!rs.next())|| (Long.getLong("RewardTimesBaum") == null));
                i = rs.getLong("RewardTimesBaum");
            }catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            createPlayerBaumReward(uuid);
            getSqlBaumReward(uuid);
        }
        return i;
    }
    public static void setSqlBaumReward(String uuid) {
        long current =  (System.currentTimeMillis() + (24*60*1000*60));
        if(playerExistsBaumReward(uuid)) {
            LobbySystem.mySQL.update("UPDATE RewardTime SET RewardTimesBaum= '" +current+ "' WHERE UUID= '" + uuid + "'");
        }else {
            createPlayerBaumReward(uuid);
            setSqlBaumReward(uuid);
        }
    }
    public static void setBaumReward(Player p ,String uuid) {
        IPermissionPlayer permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPermissionPlayer(p.getUniqueId());
        if(playerExistsBaumReward(uuid)) {
            if(permissionPlayer.getPermissionGroupInfoList().size() == 1 || permissionPlayer.hasPermissionGroup("Premium")) {
                if(permissionPlayer.getPermissionGroupInfoList().size() == 1) {
                    p.closeInventory();
                    p.sendMessage(Data.PREFIX + "§cDu kannst als §7Spieler §ckeine §2Baum §cBelohnungen abholen§8.");
                } else if(permissionPlayer.hasPermissionGroup("Premium")){
                    p.closeInventory();
                    p.sendMessage(Data.PREFIX + "§cDu kannst als §6Premium §ckeine §2Baum §cBelohnungen abholen§8.");
                }

            } else {
                if(getSqlBaumReward(uuid) < System.currentTimeMillis()) {
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 3);
                    p.sendMessage(Data.PREFIX + "§7 Dir wurden §e175 §aBäume §7dazugegeben§8.");
                    BaumAPI.getCoinsAPI().addCoins(p,175);
                    setSqlBaumReward(uuid);
                    p.closeInventory();
                } else {
                    p.playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE, 1, 3);
                    Date date = new Date(getSqlBaumReward(uuid));
                    String mm_dd_yyyy = new SimpleDateFormat("dd.MM.yyyy").format(date);
                    String hour_min = new SimpleDateFormat("HH:mm").format(date);
                    p.sendMessage(Data.PREFIX + "§cDu musst noch bis zu dem §7"+mm_dd_yyyy + " §cum §7"+ hour_min + " §cwarten§8.");
                    p.closeInventory();
                }
            }

        }else {
            createPlayerBaumReward(uuid);
            setBaumReward(p,uuid);
        }
    }
//Baum
}
