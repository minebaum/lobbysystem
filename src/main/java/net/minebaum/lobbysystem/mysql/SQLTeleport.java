package net.minebaum.lobbysystem.mysql;

import net.minebaum.lobbysystem.LobbySystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class SQLTeleport {
    public static boolean playerExists(String uuid) {
        try{
            ResultSet rs = LobbySystem.mySQL.query("SELECT * FROM Teleport WHERE UUID= '"+uuid+"'");
            if(rs.next()) {
                return rs.getString("UUID") != null;
            }
            return false;
        }catch (SQLException exception ){
            exception.printStackTrace();
        }
        return false;
    }
    public static void createPlayer(String uuid) {
        if(!(playerExists(uuid))) {
            LobbySystem.mySQL.update("INSERT INTO Teleport(UUID,STATE) VALUES ('"+uuid+"','1');");
        }
    }
    public static Integer getState(String uuid) {
        Integer i = 0;
        if(playerExists(uuid)) {
            try{
                ResultSet rs = LobbySystem.mySQL.query("Select * FROM Teleport WHERE UUID= '" + uuid + "'");
                if((!rs.next())|| (Integer.valueOf(rs.getInt("STATE")) == null));
                i = rs.getInt("STATE");
            }catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            createPlayer(uuid);
            getState(uuid);
        }
        return i;
    }
    public static void setState(String uuid ,Integer state) {
        if(playerExists(uuid)) {
            LobbySystem.mySQL.update("UPDATE Teleport SET STATE= '" + state + "' WHERE UUID= '" + uuid + "'");
        }else {
            createPlayer(uuid);
            setState(uuid,state);
        }
    }
}
